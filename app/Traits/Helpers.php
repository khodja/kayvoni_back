<?php

namespace App\Traits;

use Intervention\Image\ImageManagerStatic as Image;
use GuzzleHttp\Client;

trait Helpers
{
    public $week_map = [
        0 => 'Воскресение',
        1 => 'Понедельник',
        2 => 'Вторник',
        3 => 'Среда',
        4 => 'Четверг',
        5 => 'Пятница',
        6 => 'Суботта',
    ];
    private $endpoint = "https://fcm.googleapis.com/fcm/send";
    private $server_key = "AAAA_SwjTfc:APA91bEXcMWDFtkICBJFJmXEEIqO_EuP4Jo3pV30TtFyXeOtZh0xiHROOHox3rKI0QAgKV9T-8fMv06BRAmXokQja8xI-VH-pbbu_eUPo-INAa8HFSX5p7JZ1KzdJisGluaEYk03wrRq";
    public function imageHandle($request, $folderName, $item)
    {
        if ($request->file('image')) {
            $this->createFolder($folderName, $item->id);
            $images = $request->file('image');
            $this->storeImages($images, $item->id);
        }
    }

    public function createFolder($directory, $itemId)
    {
        $directory_path = public_path('uploads/' . $directory);
        $path = public_path('uploads/' . $directory . '/' . $itemId);
        if (!\File::isDirectory($directory_path)) {
            \File::makeDirectory($directory_path, 0777, true, true);
        }
        if (!\File::isDirectory($path)) {
            \File::makeDirectory($path, 0777, true, true);
        }
        return 1;
    }

    public function storeImages($images, $folderName, $id, $width, $height, $format = null)
    {
        foreach ($images as $key => $image) {
            $filename = $key + strtotime("now") . ($format ? '.' . $format : '.jpg');
            $path = public_path('uploads/' . $folderName . '/' . $id . '/' . $filename);
            Image::make($image->getRealPath())->encode($format ? $format : 'jpg', 100)
                ->fit($width, $height)
                ->save($path);
        }
        return 1;
    }

    public function removeImages($folderName, $id, $fileName)
    {
        $pathToDestroy = public_path('uploads/' . $folderName . '/' . $id . '/' . $fileName);
        \File::delete($pathToDestroy);
        return 1;
    }

    public function cleanDirectory($folderName, $id)
    {
        $pathToDestroy = public_path('uploads/' . $folderName . '/' . $id . '/');
        \File::cleanDirectory($pathToDestroy);
        return 1;
    }

    public function apiJsonResponse($data = null)
    {
        $response = ['statusCode' => 200];
        if ($data) {
            $response['data'] = $data;
        }
        return response()->json($response, 200);
    }

    public function getUserRestaurantId()
    {
        return \Auth::user()->restaurant_id;
    }

    public function getRestaurantUser()
    {
        return \Auth::user();
    }

    public function getUserId()
    {
        return \Auth::user()->id;
    }

    public function sendNotification($notification_token = null, $title = '', $body = '', $data = "")
    {
        if (!$notification_token) {
            return false;
        }
        $server_key = $this->server_key;
        $client = new Client([
            'headers' => [
                'Authorization' => 'key=' . $server_key,
                'Content-Type' => 'application/json'
            ]
        ]);
        $client->post($this->endpoint, [
            'json' => [
                'to' => $notification_token,
                "collapse_key" => "type_a",
                "priority" => "high",
                'notification' => [
                    'title' => $title,
                    'body' => $body,
                    'sound' => 'default'
                ],
            ]
        ]);
        return true;
    }
    public function sendMultipleNotification($notification_token = [], $title = '', $body = '', $data = "")
    {
        if (!count($notification_token)) {
            return false;
        }
        $server_key = $this->server_key;
        $client = new Client([
            'headers' => [
                'Authorization' => 'key=' . $server_key,
                'Content-Type' => 'application/json'
            ]
        ]);
        $client->post($this->endpoint, [
            'json' => [
                'registration_ids' => $notification_token,
                "collapse_key" => "type_a",
                "priority" => "high",
                'notification' => [
                    'title' => $title,
                    'body' => $body,
                    'sound' => 'default'
                ],
            ]
        ]);
        return true;
    }

//    public function sendNotification($notification_token = null, $title = '', $body = '', $data = "")
//    {
//        if (!$notification_token) {
//            return false;
//        }
//        $client = new Client();
//
//        $client->post('https://exp.host/--/api/v2/push/send', [
//            'json' => [
//                "to" => $notification_token,
//                "sound" => 'default',
//                "title" => $title,
//                "body" => $body,
//                "data" => ["data" => $data]
//            ]
//        ]);
//        return true;
//    }
}
