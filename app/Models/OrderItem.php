<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderItem extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];
    protected $appends = [];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function food()
    {
        return $this->belongsTo(Food::class);
    }

    public function params()
    {
        return $this->belongsTo(FoodParams::class, 'param_id');
    }

    public function getFoodsAttribute()
    {
//        $param_id = $this->param_id;
//        return $this->food()->with(['params' => function ($query) use ($param_id) {
//            $query->where('id', $param_id);
//        }])->first();
    }
}
