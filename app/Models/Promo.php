<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Promo extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];
    protected $appends = ['image'];

    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class);
    }

    public function foods()
    {
        return $this->belongsToMany(Food::class, 'promo_foods');
    }

    public function isActive()
    {
        $today = today()->format('Y-m-d');
        return !!$this->where('end_date', '>=', $today)->count();
    }

    public function getImageAttribute()
    {
        $directory = "uploads/promo/" . $this->id;
        $images = \File::glob($directory . "/*");
        if (count($images) > 0) {
            return asset($images[0]);
        }
        return asset('img/banner.png');
    }

}
