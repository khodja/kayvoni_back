<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Restaurant extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];
    protected $appends = ['banner', 'logo', 'rating'];

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function workdays()
    {
        return $this->hasMany(Workdays::class);
    }

    public function manager()
    {
        return $this->hasMany(User::class, 'restaurant_id', 'id');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'restaurant_categories');
    }

    public function sorts()
    {
        return $this->belongsToMany(Sort::class, 'restaurant_sorts');
    }

    public function informations()
    {
        return $this->belongsToMany(Information::class, 'restaurant_informations');
    }

    public function food()
    {
        return $this->hasMany(Food::class);
    }

    public function types()
    {
        return $this->belongsToMany(Type::class, 'restaurant_types');
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function promos()
    {
        return $this->hasMany(Promo::class);
    }

    public function getLogoAttribute()
    {
        $directory = "uploads/restaurantLogo/" . $this->id;
        $images = \File::glob($directory . "/*");
        if (count($images) > 0) {
            return asset($images[0]);
        }
        return asset('img/logo.png');
    }

    public function getBannerAttribute()
    {
        $directory = "uploads/restaurantBanner/" . $this->id;
        $images = \File::glob($directory . "/*");
        if (count($images) > 0) {
            $getImages = [];
            foreach ($images as $image) {
                array_push($getImages, asset($image));
            }
            return $getImages;
        }
        return [asset('img/banner.png')];
    }

    public function rating()
    {
        return $this->hasManyThrough(Rating::class, Order::class);
    }

    public function getRatingAttribute()
    {
        $ratings = $this->rating()->get();
        if (count($ratings) < 8) {
            return ['avg' => 5, 'count' => 0];
        }
        return ['avg' => $ratings->avg('rate'), 'count' => $ratings->count()];
    }

    public function isOpen()
    {
        $today = Carbon::now()->dayOfWeek;
        $workdays = $this->workdays[$today];
        if ($workdays->closed) {
            return false;
        }
        $open = Carbon::parse($workdays->open_time)->format('H:i:s');
        $close = Carbon::parse($workdays->close_time)->format('H:i:s');
        $time = Carbon::now()->format('H:i:s');
        if ($time >= $open || $time <= $close) {
            return true;
        }
        return false;
    }
}
