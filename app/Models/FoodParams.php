<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FoodParams extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];

    public function food()
    {
        return $this->belongsTo(Food::class);
    }

    public function order_item()
    {
        return $this->hasMany(OrderItem::class);
    }
}
