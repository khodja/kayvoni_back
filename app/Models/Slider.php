<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Slider extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];

    protected $appends = ['image'];

    public function getImageAttribute()
    {
        $directory = "uploads/slider/" . $this->id;
        $images = \File::glob($directory . "/*");
        if (count($images) > 0) {
            return asset($images[0]);
        }
        return asset('img/slider.png');
    }
}
