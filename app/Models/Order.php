<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];
    public static $statusUser = [1, 2, 3, -2, -3];
    // 0 - created | 1 - paid | 2 - accepted | 3 - done | -1 not_paid | -2 denied | -3 refunded
    public static $statusRest = [1, 2, 3, -2, -3];
    public static $statusHistory = [3, -2, -3];
    public static $statusActive = [1, 2];
    protected $appends = ['system_price', 'restaurant_price', 'real_price','seats_price','payed_amount'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class);
    }

    public function order_item()
    {
        return $this->hasMany(OrderItem::class);
    }

    public function rating()
    {
        return $this->hasOne(Rating::class);
    }

    public function getSystemPriceAttribute()
    {
        $percentage = $this->system_percentage;
        $total_food_price = $this->getRealPriceAttribute();
        return $total_food_price / 100 * $percentage;
    }

    public function getRestaurantPriceAttribute()
    {
        $percentage = $this->restaurant_percentage;
        $total_food_price = $this->getRealPriceAttribute();
        return (float) $total_food_price / 100 * $percentage;
    }
    public function getPayedAmountAttribute()
    {
        return (float) ($this->getRestaurantPriceAttribute() +  $this->getSeatsPriceAttribute() +  $this->getSystemPriceAttribute());
    }
    public function getSeatsPriceAttribute()
    {
        return (float) $this->fee_per_seat * (float) $this->seats_count;
    }
    public function getRealPriceAttribute()
    {
        return (float)$this->total_price - (float) $this->getSeatsPriceAttribute();
    }

    public function showStatus()
    {
        $status = $this->status;
        if ($status == 0) {
            return 'Создан';
        }
        if ($status == 1) {
            return 'Оплачен';
        }
        if ($status == 2) {
            return 'Принят';
        }
        if ($status == 3) {
            return 'Закрыт';
        }
        if ($status == -1) {
            return 'Не оплачен';
        }
        if ($status == -2) {
            return 'Отклонен';
        }
        if ($status == -3) {
            return 'Возврат денег';
        }
    }
}
