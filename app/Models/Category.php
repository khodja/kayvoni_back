<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];

    protected $appends = ['image'];

    public function restaurants()
    {
        return $this->belongsToMany(Restaurant::class, 'restaurant_categories');
    }
    public function food()
    {
        return $this->hasMany(Food::class);
    }

    public function getImageAttribute()
    {
        $directory = "uploads/category/" . $this->id;
        $images = \File::glob($directory . "/*");
        if (count($images) > 0) {
            return asset($images[0]);
        }
        return asset('img/category.png');
    }
}
