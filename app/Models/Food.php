<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Food extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];
    protected $appends = ['image'];

    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class);
    }

    public function params()
    {
        return $this->hasMany(FoodParams::class);
    }
    public function orderItems()
    {
        return $this->hasMany(OrderItem::class);
    }

    public function promos()
    {
        return $this->belongsToMany(Promo::class, 'promo_foods');
    }
    public function getImageAttribute()
    {
        $directory = "uploads/food/" . $this->id;
        $images = \File::glob($directory . "/*");
        if (count($images) > 0) {
            return asset($images[0]);
        }
        return asset('img/logo.png');
    }
}
