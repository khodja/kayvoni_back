<?php

namespace App\Http\Classes\Payme;

class Database
{
    public $config;

    protected static $db;

    public function __construct(array $config = null)
    {
        $this->config = $config;
    }

    public function new_connection()
    {
        $db = null;

        // connect to the database
        $db_options = [
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC, // fetch rows as associative array
            \PDO::ATTR_PERSISTENT => true // use existing connection if exists
        ];

        $db = new \PDO(
            'mysql:dbname=' . $this->config['db']['database'] . ';host=' . $this->config['db']['host'] . ';charset=utf8',
            $this->config['db']['username'],
            $this->config['db']['password'],
            $db_options
        );

        return $db;
    }

    /**
     * Connects to the database
     * @return null|\PDO connection
     */
    public static function db()
    {
    // real key = ?1qUr30?FPgrTmWohVf2mcRPto9&EvE%Aa6I
    // test key = mP3zwTZDM7Z@26GesnObo?#p3scQE56bkyu3
        if (!self::$db) {
            $config = [
                'merchant_id' => '5fd220741c849a7578de2188',
                'login' => 'Paycom',
                'keyFile' => '?1qUr30?FPgrTmWohVf2mcRPto9&EvE%Aa6I',
                'db' => [
                    'host' => '127.0.0.1',
                    'database' => 'indevuz_kayvoni',
                    'username' => 'indevuz_root',
                    'password' => 'R9M!TO?LQ=5y'
                ],
            ];
            $instance = new self($config);
            self::$db = $instance->new_connection();
        }

        return self::$db;
    }
}
