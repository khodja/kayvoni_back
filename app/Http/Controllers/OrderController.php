<?php

namespace App\Http\Controllers;

use App\Models\Food;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Restaurant;
use App\Traits\Helpers;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    use Helpers;

    private $format = "Y-m-d H:i:s";

    public function index(Request $request)
    {
        $data = Order::whereIn('status', Order::$statusRest);
        $filter = $request['filter'];
        $from = $request['from'];
        $to = $request['to'];
        if ($filter) {
            $today = Carbon::now();
            $checker = $today;
            if ($filter === 'month') {
                $checker = [$today->startOfMonth()->format($this->format), $today->endOfMonth()->format($this->format)];
            }
            if ($filter === 'week') {
                $checker = [$today->startOfWeek()->format($this->format), $today->endOfWeek()->format($this->format)];
            }
            if ($filter === 'day') {
                $checker = [$today->startOfDay()->format($this->format), $today->endOfDay()->format($this->format)];
            }
            if ($to && $from) {
                $checker = [Carbon::parse($from), Carbon::parse($to)];
            }
            $data = $data->whereBetween('reserve_time', [$checker[0], $checker[1]]);
        }
        $data = $data->paginate(15);
        return view('backend.order.index', compact('data', 'filter', 'from', 'to'));
    }

    public function get()
    {
        $user_id = $this->getUserId();
        $data = Order::where('user_id', $user_id)->with('restaurant', 'rating')->orderBy('id', 'DESC')->get();
        return $this->apiJsonResponse($data);
    }

    public function show($id)
    {
        $user_id = $this->getUserId();
        $data = Order::where([['user_id', $user_id], ['id', $id]])->with('restaurant', 'order_item.food', 'order_item.params', 'rating')->firstOrFail();
        return $this->apiJsonResponse($data);
    }

    public function checkout(Request $request)
    {
        $request->validate([
            'seats_count' => 'required',
            'reserve_time' => 'required',
            'restaurant_id' => 'required',
        ]);

        $order_food = json_decode($request['order_food'], true); /// [{food_id , param , count], [food_id , param , count] , [food_id , param , count]];
        $food_ids = []; // [[ids],[count]]
        $param_ids = [];
        $counts = [];
        foreach ($order_food as $item) {
            $food_id = $item['food_id'];
            $param_id = $item['param_id'];
            $count = $item['count'];
            array_push($food_ids, $food_id);
            array_push($param_ids, $param_id);
            array_push($counts, $count);
        }
        $orderedFood = Food::whereIn('id', $food_ids)->with(['params' => function ($query) use ($param_ids) {
            $query->whereIn('id', $param_ids);
        }, 'restaurant'])->get();
        $get_food = $orderedFood->first();
//        if (!$get_food) {
//            abort(404);
//        }
        $orderedParams = $orderedFood->pluck('params');
//        if (!$orderedParams->first()) {
//            abort(404);
//        }
        $restaurant = Restaurant::where('id', $request['restaurant_id'])->firstOrFail();
        if (count($orderedFood) > 0) {
            if ($get_food->restaurant_id != $restaurant->id) {
                abort(404, 'Restaurant is closed');
            }
        }
        $fee_per_seat = $restaurant->fee_per_seat;
        $system_percentage = $restaurant->percentage;
        $prepaid_percentage = $restaurant->prepaid_percentage;
        $reserve_time = Carbon::parse($request['reserve_time']);
        $total_price = 0;
        $orderItemData = [];
        foreach ($param_ids as $key => $param_id) {
            $findParam = $orderedParams->collapse()->where('id', $param_id)->first();
            $price = 1 * $counts[$key] * $findParam->price;
            array_push($orderItemData, [
                'food_id' => $findParam->food_id,
                'param_id' => $findParam->id,
                'count' => $counts[$key],
                'total_price' => $price,
                'price_per_count' => $findParam->price,
            ]);
            $total_price += $price;
        }
        if ($total_price < ($restaurant->deposit * 1)) {
            $total_price = (int)$restaurant->deposit;
        }
        $seats_count = $request['seats_count'] * 1;
        $user_id = $this->getUserId();
        $order = Order::create([
            'user_id' => $user_id,
            'restaurant_id' => $restaurant->id,
            'fee_per_seat' => $fee_per_seat,
            'system_percentage' => $system_percentage,
            'restaurant_percentage' => $prepaid_percentage,
            'reserve_time' => $reserve_time,
            'total_price' => $total_price + ($seats_count * $fee_per_seat),
            'seats_count' => $seats_count,
        ]);
        foreach ($orderItemData as $key => $item) {
            $data = $item;
            $data['order_id'] = $order->id;
            OrderItem::create($data);
        }
//        $tokens = \App\Models\User::where('restaurant_id', $getOrder->restaurant_id)
//            ->whereNotNull('notification_token')->pluck('notification_token')->toArray();
//        $tokens = $restaurant->manager()->whereNotNull('notification_token')->pluck('notification_token')->toArray();
//        if (count($tokens) > 0) {
//            $this->sendMultipleNotification($tokens, 'Поступил новый заказ', 'Примите его пока он актуален!');
//        }
        $prc = (int)$order->getPayedAmountAttribute();
        $last = (int)($prc * 100);
        $url = base64_encode('m=5fd220741c849a7578de2188;ac.order_id=' . $order->id . ';a=' . $last . ';c=https://kayvoni.uz');
        $data = 'https://checkout.paycom.uz/' . $url;
        return $this->apiJsonResponse($data);
    }

    public function showOrder($id)
    {
        $data = Order::where('id', $id)->with('order_item', 'restaurant', 'rating', 'user')->firstOrFail();
        return view('backend.order.show', compact('data'));
    }

/// Restaurant actions
    public
    function restGet(Request $request)
    {
        $restaurant_id = $this->getUserRestaurantId();
        $data = Order::where('restaurant_id', $restaurant_id)->whereIn('status', Order::$statusRest)->with('user')->orderBy('id', 'DESC');
        $from = $request['from'];
        $to = $request['to'];
        if ($from && $to) {
            $from = Carbon::parse($from);
            $to = Carbon::parse($to);
            $data = $data->whereBetween('reserve_time', [$from, $to]);
        }
        $data = $data->get();
        if ($request['calendar']) {
            $data = $data->mapToGroups(function ($item) {
                $parsed = Carbon::parse($item->reserve_time)->format('Y-m-d');
                return [$parsed => $item];
            });
        }
        return $this->apiJsonResponse($data);
    }

    public
    function restShow($id)
    {
        $restaurant_id = $this->getUserRestaurantId();
        $data = Order::where([['restaurant_id', $restaurant_id], ['id', $id]])->with('order_item.food', 'order_item.params', 'user')->firstOrFail();
        return $this->apiJsonResponse($data);
    }

    public
    function acceptOrder(Request $request, $id)
    {
        $restaurant_id = $this->getUserRestaurantId();
        $data = Order::where([['restaurant_id', $restaurant_id], ['id', $id]])->with('user')->firstOrFail();
        $data->update([
            'table_number' => $request['table_number'] * 1,
            'status' => 2,
        ]);
        $user = $data->user;
        if ($user) {
            if ($user->notification_token) {
                $this->sendNotification($user->notification_token, 'Ваш заказ принят', 'Ресторан вас ожидает!');
            }
        }
        return $this->apiJsonResponse();
    }

    public
    function denyOrder(Request $request, $id)
    {
        $restaurant_id = $this->getUserRestaurantId();
        $data = Order::where([['restaurant_id', $restaurant_id], ['id', $id]])->with('user')->firstOrFail();
        $data->update([
            'deny_reason' => $request['deny_reason'],
            'status' => -2,
        ]);
        $user = $data->user;
        if ($user) {
            if ($user->notification_token) {
                $this->sendNotification($user->notification_token, 'Ваш заказ отклонен', $request['deny_reason']);
            }
        }
        return $this->apiJsonResponse();
    }

    public
    function refundMoney($order)
    {
        return true;
    }
}
