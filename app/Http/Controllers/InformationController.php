<?php

namespace App\Http\Controllers;

use App\Models\Information;
use App\Traits\Helpers;
use Illuminate\Http\Request;

class InformationController extends Controller
{
    use Helpers;

    private $validation = [
        'name_uz' => 'required',
        'name_ru' => 'required',
        'name_en' => 'required',
    ];

    public function get()
    {
        $data = Information::all();
        return $this->apiJsonResponse($data);
    }

    public function index()
    {
        $data = Information::all();
        return view('backend.informations.index', compact('data'));
    }

    public function create()
    {
        return view('backend.informations.create');
    }

    public function edit($id)
    {
        $data = Information::findOrFail($id);
        return view('backend.informations.edit', compact('data'));
    }

    public function store(Request $request)
    {
        $request->validate($this->validation);
        Information::create($request->except('image', '_token'));
        return redirect()->route('informations.index')->with('success', 'Успешно добавлено');
    }

    public function update(Request $request, $id)
    {
        $request->validate($this->validation);
        $data = Information::where('id', $id)->firstOrFail();
        $data->update($request->except('image', '_token', '_method'));
        return redirect()->route('informations.index')->with('success', 'Успешно изменено');
    }
}
