<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Food;
use App\Models\Restaurant;
use App\Traits\Helpers;
use Illuminate\Http\Request;

class FoodController extends Controller
{
    use Helpers;

    public $photo_size = [
        'width' => 640,
        'height' => 360,
    ];

    public function show($id)
    {
        $data = Food::where('id', $id)->with('params')->firstOrFail();
        return $this->apiJsonResponse($data);
    }

    public function getFoods(Request $request)
    {
        $request->validate([
            'food_ids' => 'required'
        ]);
        $food_ids = json_decode($request['food_ids']);
        $data = Food::whereIn('id', $food_ids)->with('params')->get();;
        return $this->apiJsonResponse($data);
    }

    public function get()
    {
        $restaurant_id = $this->getUserRestaurantId();
        $data = Food::where('restaurant_id', $restaurant_id)->get();
        return $this->apiJsonResponse($data);
    }

    public function index($id)
    {
        $data = Food::where('restaurant_id', $id)->get();
        $restaurant = Restaurant::findOrFail($id);
        return view('backend.food.index', compact('data', 'restaurant'));
    }

    public function create($id)
    {
        $restaurant = Restaurant::where('id', $id)->with('categories')->firstOrFail();
        $categories = $restaurant->categories;
        $photo_size = $this->photo_size;
        return view('backend.food.create', compact('restaurant', 'categories', 'photo_size'));
    }

    public function edit($id)
    {
        $data = Food::findOrFail($id);
        $restaurant = Restaurant::where('id', $data->restaurant_id)->with('categories')->firstOrFail();
        $categories = $restaurant->categories;
        $directory = "uploads/food/" . $id;
        $photo_size = $this->photo_size;
        $images = \File::glob($directory . "/*");
        return view('backend.food.edit', compact('data', 'images', 'photo_size', 'categories'));
    }

    public function store(Request $request, $id)
    {
        $request->validate([
            'name_uz' => 'required',
            'name_ru' => 'required',
            'name_en' => 'required',
            'description_uz' => 'required',
            'description_ru' => 'required',
            'description_en' => 'required',
            'image' => 'required'
        ]);
        $request['restaurant_id'] = $id;
        $food = Food::create($request->except('image', '_token'));
        $this->createFolder('food', $food->id);
        $images = $request->file('image');
        $this->storeImages($images, 'food', $food->id, $this->photo_size['width'], $this->photo_size['height']);
        return redirect()->route('food.index', $id)->with('success', 'Успешно добавлено');
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name_uz' => 'required',
            'name_ru' => 'required',
            'name_en' => 'required',
            'description_uz' => 'required',
            'description_ru' => 'required',
            'description_en' => 'required',
        ]);
        $food = Food::where('id', $id)->firstOrFail();
        $food->update($request->except('image', '_token', '_method', 'restaurant_id'));
        if ($request->file('image')) {
            $this->createFolder('food', $food->id);
            $this->cleanDirectory('food', $food->id);
            $images = $request->file('image');
            $this->storeImages($images, 'food', $food->id, $this->photo_size['width'], $this->photo_size['height']);
        }
        return redirect()->route('food.index', $food->restaurant_id)->with('success', 'Успешно изменено');
    }

    public function switch($id)
    {
        $food = Food::where('id', $id)->firstOrFail();
        $this->switchFood($food);
        return redirect()->route('food.index', $food->restaurant_id)->with('success', 'Успешно изменено');
    }

    public function switchApi($id)
    {
        $restaurant_id = $this->getUserRestaurantId();
        $food = Food::where([['id', $id], ['restaurant_id', $restaurant_id]])->firstOrFail();
        $this->switchFood($food);
        return $this->apiJsonResponse();
    }

    public function switchFood($food)
    {
        return $food->update([
            'enabled' => !$food->enabled
        ]);
    }
}
