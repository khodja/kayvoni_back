<?php

namespace App\Http\Controllers;

use App\Models\Promo;
use App\Models\Restaurant;
use App\Traits\Helpers;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PromoController extends Controller
{
    use Helpers;

    public $photo_size = [
        'width' => 100,
        'height' => 100,
    ];
    private $exceptors = [
        'image', '_token', '_method', 'food_id'
    ];

    public function getToday()
    {
        return today()->format('Y-m-d');
    }

    public function show($id)
    {
        $today = $this->getToday();
        $data = Promo::where('id', $id)->where('end_date', '>=', $today)->with('foods.params')->firstOrFail(); // get only active promos
        return $this->apiJsonResponse($data);
    }

    public function restaurant($id)
    {
        $today = $this->getToday();
        $data = Promo::where([['restaurant_id', $id], ['end_date', '>=', $today]])->get();
        return $this->apiJsonResponse($data);
    }

    public function index($id)
    {
        $data = Promo::where('restaurant_id', $id)->get();
        $restaurant = Restaurant::findOrFail($id);
        return view('backend.promos.index', compact('data', 'restaurant'));
    }

    public function create($id)
    {
        $restaurant = Restaurant::where('id', $id)->with('food')->firstOrFail();
        $food = $restaurant->food;
        $photo_size = $this->photo_size;
        return view('backend.promos.create', compact('restaurant', 'food', 'photo_size'));
    }

    public function edit($id)
    {
        $data = Promo::findOrFail($id);
        $restaurant = Restaurant::where('id', $data->restaurant_id)->with('food')->firstOrFail();
        $food = $restaurant->food;
        $directory = "uploads/food/" . $id;
        $photo_size = $this->photo_size;
        $images = \File::glob($directory . "/*");
        return view('backend.promos.edit', compact('data', 'images', 'photo_size', 'food'));
    }

    public function store(Request $request, $id)
    {
        $request->validate([
            'name_uz' => 'required',
            'name_ru' => 'required',
            'name_en' => 'required',
            'description_uz' => 'required',
            'description_ru' => 'required',
            'description_en' => 'required',
            'image' => 'required',
            'food_id' => 'required'
        ]);
        $request['restaurant_id'] = $id;
        $request['start_date'] = Carbon::parse($request['start_date'])->toDateString();
        $request['end_date'] = Carbon::parse($request['end_date'])->toDateString();
        $promo = Promo::create($request->except($this->exceptors));
        $promo->foods()->attach($request['food_id']);
        $this->createFolder('promo', $promo->id);
        $images = $request->file('image');
        $this->storeImages($images, 'promo', $promo->id, $this->photo_size['width'], $this->photo_size['height']);
        return redirect()->route('promo.index', $id)->with('success', 'Успешно добавлено');
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name_uz' => 'required',
            'name_ru' => 'required',
            'name_en' => 'required',
            'description_uz' => 'required',
            'description_ru' => 'required',
            'description_en' => 'required',
            'food_id' => 'required'
        ]);
        $promo = Promo::where('id', $id)->firstOrFail();
        $promo->update($request->except($this->exceptors));
        $promo->foods()->sync($request['food_id']);
        if ($request->file('image')) {
            $this->createFolder('promo', $promo->id);
            $this->cleanDirectory('promo', $promo->id);
            $images = $request->file('image');
            $this->storeImages($images, 'promo', $promo->id, $this->photo_size['width'], $this->photo_size['height']);
        }
        return redirect()->route('promo.index', $promo->restaurant_id)->with('success', 'Успешно изменено');
    }

    public function switch($id)
    {
        $promo = Promo::where('id', $id)->firstOrFail();
        $this->switchItem($promo);
        return redirect()->route('promo.index', $promo->restaurant_id)->with('success', 'Успешно изменено');
    }

    public function switchApi($id)
    {
        $restaurant_id = $this->getUserRestaurantId();
        $promo = Promo::where([['id', $id], ['restaurant_id', $restaurant_id]])->firstOrFail();
        $this->switchItem($promo);
        return $this->apiJsonResponse();
    }

    public function switchItem($promo)
    {
        return $promo->update([
            'enabled' => !$promo->enabled
        ]);
    }
}
