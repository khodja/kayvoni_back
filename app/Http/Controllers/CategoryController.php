<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Traits\Helpers;

class CategoryController extends Controller
{
    use Helpers;

    public function get(Request $request)
    {
        $data = Category::whereHas('restaurants.food')->get();
        return $this->apiJsonResponse($data);
    }

    public function index()
    {
        $data = Category::all();
        return view('backend.category.index', compact('data'));
    }

    public function create()
    {
        return view('backend.category.create');
    }

    public function edit($id)
    {
        $data = Category::findOrFail($id);
        $directory = "uploads/category/" . $id;
        $images = \File::glob($directory . "/*");
        return view('backend.category.edit', compact('data', 'images'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name_uz' => 'required',
            'name_ru' => 'required',
            'name_en' => 'required',
            'color' => 'required',
            'image' => 'required'
        ]);

        $category = Category::create($request->except('image', '_token'));
        $this->createFolder('category', $category->id);
        $images = $request->file('image');
        $this->storeImages($images, 'category', $category->id, 120, 120, 'png');

        return redirect()->route('category.index')->with('success', 'Успешно добавлено');
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name_uz' => 'required',
            'name_ru' => 'required',
            'color' => 'required',
            'name_en' => 'required',
        ]);
        $category = Category::where('id', $id)->firstOrFail();
        $category->update($request->except('image', '_token', '_method'));
        if ($request->file('image')) {
            $this->createFolder('category', $category->id);
            $this->cleanDirectory('category', $category->id);
            $images = $request->file('image');
            $this->storeImages($images, 'category', $category->id, 120, 120, 'png');
        }
        return redirect()->route('category.index')->with('success', 'Успешно изменено');
    }
}
