<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\City;
use App\Models\Information;
use App\Models\Order;
use App\Models\Restaurant;
use App\Models\Sort;
use App\Models\Type;
use App\Traits\Helpers;
use Carbon\Carbon;
use Illuminate\Http\Request;

class RestaurantController extends Controller
{
    use Helpers;

    public $excepted = ['_token', 'category_id', '_method', 'logo', 'image', 'open_time', 'close_time', 'closed', 'information_id', 'type_id', 'sort_id'];
    public $validator = [
        'name' => 'required',
        'merchant_id' => 'required',
        'description_uz' => 'required',
        'description_ru' => 'required',
        'description_en' => 'required',
        'category_id' => 'required',
        'lat' => 'required|numeric',
        'long' => 'required|numeric',
        'district_id' => 'required',
        'deposit' => 'required',
        'percentage' => 'required',
        'seats_count' => 'required',
        'fee_per_seat' => 'required',
        'sort_id' => 'required'
    ];

    public function get()
    {
        $today = today()->format('Y-m-d');
        $restaurant = Restaurant::whereHas('food')->whereHas('categories')->whereHas('sorts')->where('enabled', true)->with(['promos', 'workdays', 'sorts'])->get();
        $promos = [];
        foreach ($restaurant as $rest) {
            $findPromo = $rest->promos->where('end_date', '>=', $today);
            if (count($findPromo)) {
                foreach ($findPromo as $promo) {
                    array_push($promos, $promo);
                }
            }
        }
        $categories = Category::take(3)->get();
        $data = ['promos' => $promos, 'restaurant' => $restaurant, 'categories' => $categories];
        return $this->apiJsonResponse($data);
    }

    public function statistics()
    {
        // 2 accepted
        $user = $this->getRestaurantUser();
        $status = Order::$statusRest;
        $restaurant = $user->restaurant()->with(['orders' => function ($query) use ($status) {
            $query->whereIn('status', $status);
        }])->firstOrFail();
        $data = $restaurant->orders()->whereIn('status', [3])->get();
        $profit = $data->sum('restaurant_price');
        $order_count = $data->count();
        $data = ['profit' => $profit, 'order_count' => $order_count, 'restaurant' => $restaurant];
        return $this->apiJsonResponse($data);
    }

    public function category($id)
    {
        $data = Restaurant::whereHas('categories', function ($query) use ($id) {
            $query->where('category_id', $id);
        })->whereHas('food')->whereHas('sorts')->with(['promos', 'workdays', 'sorts'])->get();
        return $this->apiJsonResponse($data);
    }

    public function show($id)
    {
        $data = Restaurant::where('id', $id)->with(['categories' => function ($query) {
            $query->whereHas('food');
        }, 'workdays', 'types', 'promos', 'food' => function ($query) {
            $query->where('enabled', true);
        }, 'food.params', 'sorts'])->firstOrFail();
        return $this->apiJsonResponse($data);
    }

    public function showCart($id)
    {
        $data = Restaurant::where('id', $id)->with(['workdays', 'informations'])->firstOrFail();
        return $this->apiJsonResponse($data);
    }

    public function index()
    {
        $data = Restaurant::all();
        return view('backend.restaurant.index', compact('data'));
    }

    public function create()
    {
        $districts = City::whereNull('parent_id')->with('children')->get();
        $categories = Category::all();
        $types = Type::all();
        $informations = Information::all();
        $sorts = Sort::all();
        $default_workdays = $this->getDefaultWorkdays();
        $week_map = $this->week_map;
        return view('backend.restaurant.create', compact('districts', 'categories', 'default_workdays', 'week_map', 'types', 'informations', 'sorts'));
    }

    public function edit($id)
    {
        $data = Restaurant::where('id', $id)->with('workdays')->firstOrFail();
        $workdays = $data->workdays;
        $districts = City::whereNull('parent_id')->with('children')->get();
        $categories = Category::all();
        $types = Type::all();
        $sorts = Sort::all();
        $informations = Information::all();
        $week_map = $this->week_map;
        $directory = "uploads/restaurantLogo/" . $id;
        $directoryBanner = "uploads/restaurantBanner/" . $id;
        $images = \File::glob($directory . "/*");
        $banner = \File::glob($directoryBanner . "/*");
        return view('backend.restaurant.edit', compact('data', 'sorts', 'images', 'districts', 'categories', 'banner', 'week_map', 'workdays', 'types', 'informations'));
    }

    private function getDefaultWorkdays()
    {
        $workdays = [];
        $default_workdays = [
            'open_time' => Carbon::createFromTimeString('08:00')->toTimeString(),
            'close_time' => Carbon::createFromTimeString('20:00')->toTimeString(),
        ];
        for ($i = 0; $i < 7; $i++) {
            array_push($workdays, $default_workdays);
        }
        return $workdays;
    }

    public function store(Request $request)
    {
        $request->validate(
            array_merge($this->validator, [
                'logo' => 'required',
                'image' => 'required'
            ])
        );
        $restaurant = $this->handleData($request);
        $workdays = $this->handleWorkdays($request);
        $restaurant->workdays()->createMany($workdays);
        return redirect()->route('restaurant.index')->with('success', 'Успешно добавлено');
    }

    public function handleWorkdays($request)
    {
        $workdays = [];
        $open_time = $request['open_time'];
        $close_time = $request['close_time'];
        $closed = $request['closed'];
        $week_map = $this->week_map;
        $closed_parsed = [];
        foreach ($week_map as $key => $day) {
            $checker = !!$closed ? is_bool(array_search($day, $closed)) : true;
            array_push($closed_parsed, $checker);
        }
        foreach ($open_time as $key => $open) {
            array_push($workdays, [
                'open_time' => Carbon::createFromTimeString($open_time[$key])->toTimeString(),
                'close_time' => Carbon::createFromTimeString($close_time[$key])->toTimeString(),
                'closed' => $closed_parsed[$key],
            ]);
        }
        return $workdays;
    }

    public function handleData($request)
    {
        $request['city_id'] = City::findOrFail($request['district_id'])->id;
        $restaurant = Restaurant::create($request->except($this->excepted));
        $restaurant->categories()->attach($request['category_id']);
        $restaurant->types()->attach($request['type_id']);
        $restaurant->sorts()->attach($request['sort_id']);
        $this->createFolder('restaurantLogo', $restaurant->id);
        $logo = $request->file('logo');
        $this->storeImages($logo, 'restaurantLogo', $restaurant->id, 120, 120);
        $this->createFolder('restaurantBanner', $restaurant->id);
        $banner = $request->file('image');
        $this->storeImages($banner, 'restaurantBanner', $restaurant->id, 640, 360);
        return $restaurant;
    }

    public function update(Request $request, $id)
    {
        $request->validate($this->validator);
        $restaurant = Restaurant::where('id', $id)->firstOrFail();
        $workdays = $this->handleWorkdays($request);
        $restaurant->update($request->except($this->excepted));
//        dd($is_update);
        foreach ($restaurant->workdays as $key => $inner) {
            $inner->update($workdays[$key]);
        }
        if ($request['category_id']) {
            $restaurant->categories()->sync($request['category_id']);
        }
        if ($request['type_id']) {
            $restaurant->types()->sync($request['type_id']);
        }
        if ($request['sort_id']) {
            $restaurant->sorts()->sync($request['sort_id']);
        }
        if ($request->file('logo')) {
            $this->createFolder('restaurantLogo', $restaurant->id);
            $this->cleanDirectory('restaurantLogo', $restaurant->id);
            $banner = $request->file('logo');
            $this->storeImages($banner, 'restaurantLogo', $restaurant->id, 120, 120);
        }
        if ($request->file('image')) {
            $this->createFolder('restaurantBanner', $restaurant->id);
            $this->cleanDirectory('restaurantBanner', $restaurant->id);
            $images = $request->file('image');
            $this->storeImages($images, 'restaurantBanner', $restaurant->id, 640, 360);
        }
        return redirect()->route('restaurant.index')->with('success', 'Успешно изменено');
    }

    public function switch($id)
    {
        $restaurant = Restaurant::where('id', $id)->firstOrFail();
        $restaurant->update([
            'enabled' => !$restaurant->enabled
        ]);
        return redirect()->route('restaurant.index')->with('success', 'Успешно изменено');
    }

    public function switchRecommend($id)
    {
        $restaurant = Restaurant::where('id', $id)->firstOrFail();
        $restaurant->update([
            'recommend' => !$restaurant->recommend
        ]);
        return redirect()->route('restaurant.index')->with('success', 'Успешно изменено');
    }

    public function removeImage($alias)
    {
        $file_name = request('file_name');
        $pathToDestroy = public_path('uploads/restaurantBanner/' . $alias . '/' . $file_name);
        \File::delete($pathToDestroy);
        return response('okay', 200);
    }
}
