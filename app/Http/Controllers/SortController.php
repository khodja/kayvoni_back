<?php

namespace App\Http\Controllers;

use App\Models\Sort;
use App\Traits\Helpers;
use Illuminate\Http\Request;

class SortController extends Controller
{
    use Helpers;

    private $validation = [
        'name_uz' => 'required',
        'name_ru' => 'required',
        'name_en' => 'required',
    ];

    public function get()
    {
        $data = Sort::all();
        return $this->apiJsonResponse($data);
    }

    public function index()
    {
        $data = Sort::all();
        return view('backend.sort.index', compact('data'));
    }

    public function create()
    {
        return view('backend.sort.create');
    }

    public function edit($id)
    {
        $data = Sort::findOrFail($id);
        return view('backend.sort.edit', compact('data'));
    }

    public function store(Request $request)
    {
        $request->validate($this->validation);
        Sort::create($request->except('image', '_token'));
        return redirect()->route('sort.index')->with('success', 'Успешно добавлено');
    }

    public function update(Request $request, $id)
    {
        $request->validate($this->validation);
        $data = Sort::where('id', $id)->firstOrFail();
        $data->update($request->except('image', '_token', '_method'));
        return redirect()->route('sort.index')->with('success', 'Успешно изменено');
    }
}
