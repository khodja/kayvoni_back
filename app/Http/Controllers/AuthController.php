<?php

namespace App\Http\Controllers;


use App\Models\Phone;
use App\Models\Order;
use App\Models\Role;
use App\Models\User;
use App\Models\Message;
use App\Traits\Helpers;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Auth;

class AuthController extends Controller
{
    use Helpers;

    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);
        $user = Auth::user();
        $user->update([
            'name' => $request['name'],
        ]);
        return $this->apiJsonResponse();
    }

    public function auth(Request $request)
    {
        $request->validate([
            'phone' => 'required',
            'verify_code' => 'required',
        ]);
        $checkPassword = Phone::where([['phone', request('phone')], ['code', request('verify_code')]])->first();
        $checkForExist = User::where('phone', request('phone'))->first();
        if (is_null($checkPassword)) {
            return response()->json(['statusCode' => 404, 'message' => 'Неправильные данные'], 404);
        }
        if (is_null($checkForExist)) {
            $user = User::create([
                'phone' => request('phone'),
                'name' => 'Kayvoni User',
                'password' => bcrypt(request('verify_code')),
            ]);
            $role = Role::where('type', 'member')->first();
            $user->roles()->attach($role);
        } else {
            $user = $checkForExist;
        }
        $token = $user->createToken($user->phone);
        $data = [
            'token' => $token->accessToken,
        ];
        return $this->apiJsonResponse($data);
    }

    public function login()
    {
        $user = User::where(['phone' => request('phone')])->firstOrFail();
        if (\Hash::check(request('password'), $user->password)) {
            $token = $user->createToken($user->phone);
        } else {
            $response = "Password missmatch";
            return response()->json($response, 422);
        }
        return response()->json($token, 200);
    }

    public function restLogin()
    {
        $user = User::where(['phone' => request('phone')])->whereHas('roles', function ($query) {
            $query->where('type', 'restaurant');
        })->firstOrFail();
        if (\Hash::check(request('password'), $user->password)) {
            $token = $user->createToken($user->phone);
            return response()->json($token, 200);
        } else {
            $response = "Password missmatch";
            return response()->json($response, 422);
        }
    }

    public function setToken(Request $request)
    {
        $user = Auth::user();
        if ($request['notification_token']) {
            if ($request['notification_token'] != $user->notification_token) {
                $user->update([
                    'notification_token' => $request['notification_token']
                ]);
            }
        }
        if ($request['name']) {
            $user->update([
                'name' => $request['name']
            ]);
        }
        return $this->apiJsonResponse();
    }

    public function verification(Request $request)
    {
        $request->validate([
            'phone' => 'required'
        ]);
        $password = rand(1000, 9999);
        if ($request['phone'] == '990008991') $password = 1234;
        $success = $this->send_code($request['phone'], $password);
        Phone::updateOrCreate(['phone' => $request['phone']], [
            'phone' => $request['phone'],
            'code' => $password,
        ]);
        if (!$success) {
            return response()->json(['statusCode' => 404, 'message' => 'sms_not_working'], 404);
        }
        return $this->apiJsonResponse();
    }

    public function send_code($phone, $password)
    {
        $verify_password = $password;
        $client = new Client();
        $token = $this->getToken();
        $headers = [
            'Authorization' => 'Bearer ' . $token,
        ];
        $client->post('notify.eskiz.uz/api/message/sms/send', [
            'headers' => $headers,
            'form_params' => [
                "mobile_phone" => "998".$phone,
                "message" => "Ваш код подтверждения: " . $verify_password . '. Наберите его в поле ввода.'
            ]
        ]);
        return true;
    }
    public function getToken() {
        $today = Carbon::yesterday();
        $latest_message =  Message::latest()->first();
        $from = Carbon::parse($latest_message->created_at);
        $diff_in_days = $from->diffInDays($today);
        if($diff_in_days < 20){
            return $latest_message->token;
        }
        $client = new Client();
        $request = $client->post('notify.eskiz.uz/api/auth/login', [
            "form_params"=> [
                "email" => "info@ipakyuli.com",
                "password" => "Mcxy5MTmi72mgU5AP8uMx8OFMTuGKJQaWMQ4xDz7",
            ]
        ]);
        $response = $request->getBody();
        $response = json_decode($response , true);
        $token = $response['data']['token'];
        Message::create([
            'token' => $token
        ]);
        return $token;

    }

    public function getUser()
    {
        $user = Auth::user();
        $user = User::where('id', $user->id)->firstOrFail();
        return $this->apiJsonResponse($user);
    }

    public function getRestaurantUser()
    {
        $user = Auth::user();
        $user = User::where('id', $user->id)->firstOrFail();
        if (!$user->restaurant_id) {
            abort(404);
        }
        $filter = Order::$statusRest;
        $active_orders = Order::where('restaurant_id', $user->restaurant_id)->whereIn('status', $filter)->count();
        $data = ['user' => $user, 'orders_count' => $active_orders];
        return $this->apiJsonResponse($data);
    }

    public function logout()
    {
        Auth::user()->token()->revoke();
        return $this->apiJsonResponse();
    }

}
