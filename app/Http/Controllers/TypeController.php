<?php

namespace App\Http\Controllers;

use App\Models\Type;
use App\Traits\Helpers;
use Illuminate\Http\Request;

class TypeController extends Controller
{
    use Helpers;

    public $photoSize = [
        'width' => 100,
        'height' => 100,
    ];

    private $validation = [
        'name_uz' => 'required',
        'name_ru' => 'required',
        'name_en' => 'required',
    ];

    public function get()
    {
        $data = Type::all();
        return $this->apiJsonResponse($data);
    }

    public function index()
    {
        $data = Type::all();
        return view('backend.type.index', compact('data'));
    }

    public function create()
    {
        return view('backend.type.create');
    }

    public function edit($id)
    {
        $data = Type::findOrFail($id);
        $directory = "uploads/types/" . $id;
        $images = \File::glob($directory . "/*");
        return view('backend.type.edit', compact('data', 'images'));
    }

    public function store(Request $request)
    {
        $request->validate($this->validation);
        $type = Type::create($request->except('image', '_token'));
        $this->createFolder('types', $type->id);
        $images = $request->file('image');
        $this->storeImages($images, 'types', $type->id, $this->photoSize['width'], $this->photoSize['height']);
        return redirect()->route('type.index')->with('success', 'Успешно добавлено');
    }

    public function update(Request $request, $id)
    {
        $request->validate($this->validation);
        $type = Type::where('id', $id)->firstOrFail();
        $type->update($request->except('image', '_token', '_method'));
        if ($request->file('image')) {
            $this->createFolder('types', $type->id);
            $this->cleanDirectory('types', $type->id);
            $images = $request->file('image');
            $this->storeImages($images, 'types', $type->id, $this->photoSize['width'], $this->photoSize['height']);
        }
        return redirect()->route('type.index')->with('success', 'Успешно изменено');
    }
}
