<?php

namespace App\Http\Controllers;

use App\Models\Food;
use App\Models\Restaurant;
use App\Traits\Helpers;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    use Helpers;

    public function get(Request $request)
    {
        $keyword = $request['keyword'];
        $category_ids = $request['category_ids'];
        $price_range = $request['price_range'];
        $types = $request['types_ids'];
        $sorts = $request['sort_ids'];
        $restaurants = Restaurant::with(['workdays', 'categories'])->where('enabled', true);
        $food = Food::where('enabled', true)->whereHas('params')->whereHas('restaurant', function ($query) {
            $query->where('enabled', true);
        })->with('params');
        if ($keyword) {
            $food = $food->where('name_ru', 'like', '%' . $keyword . '%')->orWhere('name_uz', 'like', '%' . $keyword . '%')->orWhere('name_en', 'like', '%' . $keyword . '%');
            $restaurants = $restaurants->where('name', 'like', '%' . $keyword . '%');
        }
        if ($category_ids) {
            $category_ids = json_decode($category_ids);
            $food = $food->whereIn('category_id', $category_ids);
        }
        if ($price_range) {
            $range = explode(',', $price_range);
            $restaurants = $restaurants->where('deposit', '>=', $range[0] * 1)->where('deposit', '<=', $range[1] * 1);
        }
        if ($types) {
            $restaurants = $restaurants->whereHas('types', function ($query) use ($types) {
                $query->whereIn('id', $types);
            });
        }
        if ($types) {
            $restaurants = $restaurants->whereHas('sorts', function ($query) use ($sorts) {
                $query->whereIn('id', $sorts);
            });
        }
        $restaurants = $restaurants->get();
        $food = $food->get();
        $data = ['restaurants' => $restaurants, 'food' => $food];
        return $this->apiJsonResponse($data);
    }
}
