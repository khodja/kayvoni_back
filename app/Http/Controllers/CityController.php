<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Traits\Helpers;
use Illuminate\Http\Request;

class CityController extends Controller
{
    use Helpers;
    private $validation = [
        'name_uz' => 'required',
        'name_ru' => 'required',
        'name_en' => 'required',
    ];

    public function get()
    {
        $data = City::all();
        $this->apiJsonResponse($data);
    }

    public function index()
    {
        $data = City::whereNull('parent_id')->get();
        return view('backend.city.index', compact('data'));
    }

    public function show($id)
    {
        $data = City::where('parent_id', $id)->with('children', 'parent')->get();
        $parent = City::findOrFail($id);
        return view('backend.city.index', compact('data', 'parent'));
    }

    public function create()
    {
        $data = City::all();
        return view('backend.city.create', compact('data'));
    }

    public function edit($id)
    {
        $data = City::findOrFail($id);
        $categories = City::where('id', '!=', $id)->get();
        return view('backend.city.edit', compact('data', 'categories'));
    }

    public function store(Request $request)
    {
        $request->validate($this->validation);
        if ($request['parent_id'] == 0) {
            $request['parent_id'] = null;
        }

        $category = City::create($request->except('_token'));
        if ($category->parent_id !== null) {
            return redirect()->route('city.show', $category->parent_id)->with('success',
                'Успешно добавлено');
        } else {
            return redirect()->route('city.index')
                ->with('success', 'Успешно добавлено');
        }
    }

    public function update(Request $request, $id)
    {
        $request->validate($this->validation);
        if ($request['parent_id'] == 0) {
            $request['parent_id'] = null;
        }
        $category = City::findOrFail($id);
        $category->update($request->except('_token', '_method'));
        if ($category->parent_id !== null) {
            return redirect()->route('city.show', $category->parent_id)->with('success',
                'Успешно добавлено');
        } else {
            return redirect()->route('city.index')
                ->with('success', 'Успешно добавлено');
        }
    }

    public function toggleMain($id)
    {
        $data = City::findOrFail($id);
        $data->update([
            'main' => !$data->main
        ]);
        return redirect()->back();
    }
}
