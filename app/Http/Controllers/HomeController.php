<?php

namespace App\Http\Controllers;

use App\Models\Food;
use App\Models\Order;
use App\Models\Restaurant;
use App\Models\User;
use App\Traits\Helpers;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
class HomeController extends Controller
{
    use Helpers;
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $orders = Order::whereIn('status', Order::$statusActive)->get();
        $users = User::where('id', '!=', 1)->get();
        $foods = Food::withCount('orderItems')->orderBy('order_items_count', 'DESC')->get();
        $restaurants = Restaurant::all();
        return view('backend.index', compact('orders', 'users', 'restaurants', 'foods'));
    }

    public function logout(Request $request)
    {
        \Auth::logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/login');
    }

    public function refresh()
    {
//        $this->sendMultipleNotification(['eMjsxIDrjFldfzj9F52MND:APA91bHT5IYO1KiZ_Ndn5m85Kpgy1VUKnUjn2onqTl963GTrozfXIJExOr1zhITlrEubbiRN16vtBLSsKAGDDlyW6DAevZ0Now3kREKUkf1Ok-YzNLFUJBXWHisN2eGfNo2FDzlHSEW4'],'test','test');

        \Artisan::call('optimize');
//        \Artisan::call('optimize:clear');
        return redirect()->back();
    }

    public function clearCache(){
        \Artisan::call('cache:clear');
        return redirect()->back();
    }
    public function clearRoute(){
        \Artisan::call('route:clear');
        return redirect()->back();
    }
    public function clearView(){
        \Artisan::call('view:clear');
        return redirect()->back();
    }
    public function clearConfig(){
        \Artisan::call('config:cache');
        return redirect()->back();
    }
    public function clearOptimize(){
        \Artisan::call('optimize:clear');
        return redirect()->back();
    }
}
