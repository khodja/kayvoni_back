<?php

namespace App\Http\Controllers;

use App\Models\Food;
use App\Models\FoodParams;
use Illuminate\Http\Request;

class FoodParamsController extends Controller
{
    public function index($id)
    {
        $data = FoodParams::where('food_id', $id)->get();
        $food = Food::findOrFail($id);
        return view('backend.params.index', compact('data', 'food'));
    }

    public function create($id)
    {
        $food = Food::where('id', $id)->firstOrFail();
        return view('backend.params.create', compact('food'));
    }

    public function edit($id)
    {
        $data = FoodParams::findOrFail($id);
        $food = Food::where('id', $data->food_id)->firstOrFail();
        return view('backend.params.edit', compact('data', 'food'));
    }

    public function store(Request $request, $id)
    {
        $request->validate([
            'name_uz' => 'required',
            'name_ru' => 'required',
            'name_en' => 'required',
            'price' => 'required'
        ]);
        $request['food_id'] = $id;
        FoodParams::create($request->except('image', '_token'));
        return redirect()->route('params.index', $id)->with('success', 'Успешно добавлено');
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name_uz' => 'required',
            'name_ru' => 'required',
            'name_en' => 'required',
            'price' => 'required'
        ]);
        $food = FoodParams::where('id', $id)->firstOrFail();
        $food->update($request->except('image', '_token', '_method', 'food_id'));
        return redirect()->route('params.index', $food->food_id)->with('success', 'Успешно изменено');
    }
}
