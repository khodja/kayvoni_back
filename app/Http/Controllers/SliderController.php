<?php

namespace App\Http\Controllers;

use App\Models\Slider;
use App\Traits\Helpers;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    use Helpers;

    public function get()
    {
        $data = Slider::all();
        return $this->apiJsonResponse($data);
    }

    public function index()
    {
        $data = Slider::all();
        return view('backend.slider.index', compact('data'));
    }
    public function policy()
    {
        $data = Slider::all();
        return view('backend.slider.policy', compact('data'));
    }

    public function create()
    {
        return view('backend.slider.create');
    }

    public function edit($id)
    {
        $data = Slider::find($id);
        $directory = "uploads/slider/" . $id;
        $images = \File::glob($directory . "/*");
        return view('backend.slider.edit', compact('data', 'id', 'images'));
    }

    public function store(Request $request)
    {
        $slider = Slider::create();
        $this->createFolder('slider', $slider->id);
        $images = $request->file('image');
        $this->storeImages($images, 'slider', $slider->id, 640, 360, 'jpg');
        return redirect()->route('slider.index')->with('success', 'Успешно добавлено');
    }

    public function update(Request $request, $id)
    {
        $slider = Slider::findOrFail($id);
        $slider->update($request->except('image', 'method'));
        if ($request->file('image')) {
            $this->createFolder('slider', $slider->id);
            $this->cleanDirectory('slider', $slider->id);
            $images = $request->file('image');
            $this->storeImages($images, 'slider', $slider->id, 640, 360, 'jpg');
        }
        return redirect()->route('slider.index')->with('success', 'Изменения успешно внесены');
    }

    public function delete($id)
    {
        $slider = Slider::findOrFail($id);
        $slider->delete();
        return redirect()->route('slider.index')->with('success', 'Успешно удален');
    }
}
