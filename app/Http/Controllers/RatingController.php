<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Rating;
use App\Traits\Helpers;
use Illuminate\Http\Request;

class RatingController extends Controller
{
    use Helpers;

    public function store(Request $request, $id)
    {
        $user_id = $this->getUserId();
        $order = Order::where([['id', $id], ['user_id', $user_id]])->firstOrFail();
        $check_for = Rating::where(['order_id' => $order->id, 'user_id' => $user_id])->first();
        if (!$check_for) {
            Rating::create([
                'order_id' => $order->id,
                'user_id' => $user_id,
                'rate' => $request['rate'] * 1,
                'feedback' => $request['feedback'] ?? '',
            ]);
            return $this->apiJsonResponse();
        }
        abort(404);
    }
}
