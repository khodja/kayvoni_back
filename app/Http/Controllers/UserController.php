<?php

namespace App\Http\Controllers;

use App\Models\Restaurant;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Auth;

class UserController extends Controller
{
    public $excepted = ['image', '_token', '_method'];

    public function index()
    {
        $data = User::with('orders')->whereHas('roles', function ($query) {
            $query->where('type', 'restaurant');
        })->get();
        $title = 'Managers';
        return view('backend.user.index', compact('data', 'title'));
    }

    public function restaurant($id)
    {
        $data = User::with('orders')->whereHas('roles', function ($query) {
            $query->where('type', 'restaurant');
        })->where('restaurant_id', $id)->get();
        $restaurant = Restaurant::find($id);
        $title = 'Managers of restaurant '. $restaurant->name;
        return view('backend.user.index', compact('data', 'title'));
    }

    public function create()
    {
        $roles = Role::all();
        $restaurants = Restaurant::all();
        return view('backend.user.create', compact('roles', 'restaurants'));
    }

    public function edit($id)
    {
        $data = User::with('orders')->whereHas('roles', function ($query) {
            $query->where('type', 'restaurant');
        })->where('id', $id)->firstOrFail();
        $roles = Role::all();
        $restaurants = Restaurant::all();
        return view('backend.user.edit', compact('roles', 'restaurants', 'data'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'phone' => 'required',
            'password' => 'required',
            'restaurant_id' => 'required'
        ]);
        $request['password'] = bcrypt($request['password']);
        $merged_role = Role::where('type', 'restaurant')->first();
        $manager = User::create($request->except($this->excepted));
        $manager->roles()->attach($merged_role);
        return redirect()->route('manager.index')->with('success', 'Успешно добавлено');
    }

    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $exceptor = $this->excepted;
        if (!$request->password) {
            array_push($exceptor, 'password');
        } else {
            $request['password'] = bcrypt($request['password']);
        }
        $user->update($request->except($exceptor));
        return redirect()->route('manager.index')->with('success', 'Успешно');
    }
}
