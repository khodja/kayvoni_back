<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('restaurant_id');
            $table->integer('status')->default(0); // 0 - created | 1 - paid | 2 - accepted | 3 - done | -1 not_paid | -2 denied | -3 refunded
            $table->unsignedInteger('seats_count');
            $table->unsignedInteger('table_number')->nullable();
            $table->string('deny_reason')->nullable();
            $table->string('description')->nullable();
            $table->unsignedInteger('fee_per_seat');
            $table->unsignedInteger('state')->default(0);
            $table->unsignedInteger('system_percentage');
            $table->unsignedInteger('restaurant_percentage');
            $table->unsignedInteger('total_price');
            $table->dateTime('reserve_time');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
