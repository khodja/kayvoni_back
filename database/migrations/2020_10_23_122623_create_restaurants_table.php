<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestaurantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurants', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('description_uz', 255);
            $table->string('description_ru', 255);
            $table->string('description_en', 255);
            $table->text('merchant_id');
            $table->unsignedInteger('city_id');
            $table->unsignedInteger('district_id');
            $table->unsignedInteger('deposit')->default(0);
            $table->unsignedFloat('percentage', 10, 5)->default(0);
            $table->unsignedFloat('prepaid_percentage', 10, 5)->default(0);
            $table->unsignedInteger('seats_count');
            $table->unsignedInteger('fee_per_seat');
            $table->decimal('long', 10, 7);
            $table->decimal('lat', 10, 7);
            $table->boolean('recommend')->default(false);
            $table->boolean('enabled')->default(false);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurants');
    }
}
