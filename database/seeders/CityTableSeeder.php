<?php

namespace Database\Seeders;

use App\Models\City;
use Illuminate\Database\Seeder;

class CityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $parent = City::create([
            'name_ru' => 'Ташкент',
            'name_uz' => 'Toshkent',
            'name_en' => 'Tashkent',
        ]);
        City::create([
            'name_ru' => 'Яккасарой',
            'name_uz' => 'Yakkasaroy',
            'name_en' => 'Yakkasaray',
            'parent_id' => $parent->id,
        ]);
    }
}
