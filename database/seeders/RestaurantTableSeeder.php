<?php

namespace Database\Seeders;

use App\Models\Restaurant;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class RestaurantTableSeeder extends Seeder
{
    private function getDefaultWorkdays()
    {
        $workdays = [];
        $default_workdays = [
            'open_time' => Carbon::createFromTimeString('08:00')->toTimeString(),
            'close_time' => Carbon::createFromTimeString('20:00')->toTimeString(),
        ];
        for ($i = 0; $i < 7; $i++) {
            array_push($workdays, $default_workdays);
        }
        return $workdays;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        Restaurant::create([
//
//        ]);
    }
}
