@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <h2 class="blue-title">Рестораны</h2>
            <div class="row">
                <div class="form">
                    <div class="white-block">
                        <ul class="list-block">
                            @foreach ($data as $restaurant)
                                <li class="item">
                                    <div class="left-block">
                                        <i class="fe fe-list"></i> {{ $restaurant->name }}
                                    </div>
                                    <div class="right-block">
                                        <label class="custom-switch mr-1">
                                            <span class="custom-switch-description mr-1">OFF/ON</span>
                                            <input type="checkbox" name="custom-switch-checkbox"
                                                   class="custom-switch-input"
                                                   @if($restaurant->enabled) checked="checked" @endif>
                                            <span class="custom-switch-indicator actioner"
                                                  data-href="{{route('restaurant.switch',$restaurant->id)}}"></span>
                                        </label>
                                        <a href="{{ route('food.index',$restaurant->id) }}"
                                           class="btn btn-outline-primary mr-1">Меню</a>
                                        <a href="{{ route('manager.restaurant',$restaurant->id) }}"
                                           class="btn btn-outline-primary mr-1">Менеджеры</a>
                                        <a href="{{ route('promo.index',$restaurant->id) }}"
                                           class="btn btn-outline-primary mr-1">Акции</a>
                                        <a href="{{ route('restaurant.edit',$restaurant->id) }}"
                                           class="btn btn-outline-primary">Изменить</a>
                                    </div>
                                </li>
                            @endforeach
                            <a class="add-list-btn" href="{{ route('restaurant.create') }}">
                                <i class="fe fe-plus-circle"></i>
                                Добавить Ресторан
                            </a>
                        </ul>
                    </div>
                    <div class="button-block">
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        $('.actioner').on('click', function () {
            let url = $(this).data('href');
            $.get(url);
        })
    </script>
@endsection
