@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <form action="{{ route('restaurant.store') }}" method="POST" enctype="multipart/form-data" class="form">
                    @csrf
                    <div class="white-block mb-30">
                        <div class="head">
                            <h3>Добавить Ресторан</h3>
                        </div>
                        <div class="content">
                            <div class="input-block">
                                <div class="input">
                                    <label for="name">Название</label>
                                    <input required="required" type="text" name="name" value="{{old('name')}}"
                                           class="form-control regStepOne" id="name" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="merchant_id">Merchant ID</label>
                                    <input required="required" type="text" name="merchant_id"
                                           value="{{old('merchant_id')}}"
                                           class="form-control regStepOne" id="merchant_id" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="district_id">Регион</label>
                                    <select name="district_id" id="district_id" class="form-control"
                                            required="required">
                                        <option disabled selected>Не выбран</option>
                                        @foreach($districts as $district)
                                            <optgroup label="{{$district->name_ru}}">
                                                @foreach($district->children as $child)
                                                    <option value="{{$child->id}}">{{$child->name_ru}}</option>
                                                @endforeach
                                            </optgroup>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="category">Выберите категорию</label>
                                    <select multiple name="category_id[]" required id="category"
                                            class="chosen-select form-control">
                                        @foreach( $categories as $datas )
                                            <option value="{{ $datas->id }}">{{ $datas->name_ru }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="category">Выберите тип ресторана</label>
                                    <select multiple name="sort_id[]" required id="category"
                                            class="chosen-select form-control">
                                        @foreach( $sorts as $datas )
                                            <option value="{{ $datas->id }}">{{ $datas->name_ru }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="type_id">Выберите Информацию ресторана</label>
                                    <select multiple name="type_id[]" required id="type_id"
                                            class="chosen-select form-control">
                                        @foreach( $types as $datas )
                                            <option value="{{ $datas->id }}">{{ $datas->name_ru }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="description_ru">Описание (RU)</label>
                                    <input required="required" type="text" name="description_ru"
                                           value="{{old('description_ru')}}"
                                           class="form-control regStepOne" id="description_ru" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="description_uz">Описание (UZ)</label>
                                    <input required="required" type="text" name="description_uz"
                                           value="{{old('description_uz')}}"
                                           class="form-control regStepOne" id="description_uz" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="description_en">Описание (EN)</label>
                                    <input required="required" type="text" name="description_en"
                                           value="{{old('description_en')}}"
                                           class="form-control regStepOne" id="description_en" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="seats_count">Кол-во мест (шт)</label>
                                    <input required="required" type="number" min="1" name="seats_count"
                                           value="{{old('seats_count')}}"
                                           class="form-control regStepOne" id="seats_count" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="deposit">Мин заказ (UZS)</label>
                                    <input required="required" type="number" min="1" name="deposit"
                                           value="{{old('deposit')}}"
                                           class="form-control regStepOne" id="deposit" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="prepaid_percentage">Процент предоплаты для ресторана %</label>
                                    <input required="required" type="number" min="0" name="prepaid_percentage"
                                           value="{{old('prepaid_percentage')}}"
                                           class="form-control regStepOne" id="prepaid_percentage" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="percentage">Процент системы %</label>
                                    <input required="required" type="number" min="0" step="0.001" name="percentage"
                                           value="{{old('percentage')}}"
                                           class="form-control regStepOne" id="percentage" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="fee_per_seat">Цена системы за место (UZS)</label>
                                    <input required="required" type="number" min="1" name="fee_per_seat"
                                           value="{{old('fee_per_seat')}}"
                                           class="form-control regStepOne" id="fee_per_seat" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block pb-3 jv_map_register">
                                <div class="input">
                                    <label>Локация:</label>
                                    <div id="map" style="height: 400px; width: 100%;"></div>
                                    <input id="long" value="69.2405621" name="long" class="form-control"/>
                                    <input id="lat" value="41.3110811" name="lat" class="form-control"/>
                                </div>
                            </div>
                            <div class="input-block">
                                @foreach($default_workdays as $key => $workdays)
                                    <div class="col-lg-12 p-0 d-flex align-items-center">
                                        <div style="min-width: 140px">
                                            <label class="form-check form-check-inline">
                                                <input class="form-check-input schedule-handler" name="closed[]"
                                                       value="{{$week_map[$key]}}" checked="checked" type="checkbox">
                                                <span class="form-check-label">{{$week_map[$key]}}</span>
                                            </label>
                                        </div>
                                        <input type="time" class="time" name="open_time[]" style="width: 80px;"
                                               value="{{$workdays['open_time']}}"/>
                                        <div style="width: 10px;"></div>
                                        <input type="time" class="time" name="close_time[]" style="width: 80px;"
                                               value="{{$workdays['close_time']}}"/>
                                    </div>
                                @endforeach
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label>Logo 120x120:</label>
                                    <input type="file" required="required" name="logo[]">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="white-block mb-30">
                        <div class="head">
                            <h3>Баннер 640x360</h3>
                        </div>
                        <div class="card-header-after">
                            <p></p>
                            <div class="fotoUploader">
                                <input required="required" type="file" name="image[]" class="filer_input2"
                                       multiple="multiple">
                            </div>
                        </div>
                    </div>
                    <div class="button-block">
                        <button type="submit" class="continue-btn">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
@section('style')
    <link rel="stylesheet" href="{{asset('backend/css/chosen.css')}}">
@endsection
@section('script')
    <script async defer src="https://maps.googleapis.com/maps/api/js?callback=initMap"></script>
    <script src="{{ asset('backend/js/vendors/chosen.js') }}"></script>
    <script>
        let latitude = $('#lat');
        let longitude = $('#long');
        let scheduleHandler = $('.schedule-handler');

        function weekHandler() {
            scheduleHandler.change(function () {
                $(this).parent().parent().parent().find('.time').toggleClass('disabled');
            })
        }
        function initMap() {
            let latlng = new google.maps.LatLng(41.311081, 69.240562);
            let map = new google.maps.Map(document.getElementById('map'), {
                center: latlng,
                zoom: 12,
                animation: google.maps.Animation.BOUNCE
            });
            let marker = new google.maps.Marker({
                position: latlng,
                map: map,
                draggable: true
            });
            let lat, long;
            google.maps.event.addListener(marker, 'dragend', function (event) {
                lat = this.getPosition().lat().toFixed(7);
                long = this.getPosition().lng().toFixed(7);
                // console.log(lat + 'lat' + long + 'lng')
                latitude.val(lat);
                longitude.val(long);
            });
        }

        $(function () {
            $('.chosen-select').chosen();
            weekHandler();
        });
    </script>
@endsection
