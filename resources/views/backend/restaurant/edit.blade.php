@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <form action="{{ route('restaurant.update',$data->id) }}" method="POST"
                      enctype="multipart/form-data" class="form">
                    @csrf
                    @method('PUT')
                    <div class="white-block mb-30">
                        <div class="head">
                            <h3>Изменить Ресторан</h3>
                        </div>
                        <div class="content">
                            <div class="input-block">
                                <div class="input">
                                    <label for="name">Название</label>
                                    <input required="required" type="text" name="name" value="{{$data->name}}"
                                           class="form-control regStepOne" id="name" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="merchant_id">Merchant ID</label>
                                    <input required="required" type="text" name="merchant_id" value="{{$data->merchant_id}}"
                                           class="form-control regStepOne" id="merchant_id" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="district_id">Регион</label>
                                    <select name="district_id" id="district_id" class="form-control"
                                            required="required">
                                        @foreach($districts as $district)
                                            <optgroup label="{{$district->name_ru}}">
                                                @foreach($district->children as $child)
                                                    <option value="{{$child->id}}"
                                                            @if( $child->id == $data->district_id) selected @endif>
                                                        {{$child->name_ru}}
                                                    </option>
                                                @endforeach
                                            </optgroup>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="category">Выберите категорию</label>
                                    <select multiple name="category_id[]" required id="category"
                                            class="chosen-select form-control">
                                        @foreach( $categories as $datas )
                                            <option value="{{ $datas->id }}"
                                                    @if( in_array($datas->id, $data->categories->pluck('id')->toArray())) selected @endif>{{ $datas->name_ru }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="category">Выберите тип ресторана</label>
                                    <select multiple name="sort_id[]" required id="category"
                                            class="chosen-select form-control">
                                        @foreach( $sorts as $datas )
                                            <option value="{{ $datas->id }}"
                                                    @if( in_array($datas->id, $data->sorts->pluck('id')->toArray())) selected @endif>{{ $datas->name_ru }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="type_id">Выберите Информацию ресторана</label>
                                    <select multiple name="type_id[]" required id="type_id"
                                            class="chosen-select form-control">
                                        @foreach( $types as $datas )
                                            <option value="{{ $datas->id }}"
                                                    @if( in_array($datas->id, $data->types->pluck('id')->toArray())) selected @endif>{{ $datas->name_ru }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="description_ru">Описание (RU)</label>
                                    <input required="required" type="text" name="description_ru"
                                           value="{{$data->description_ru}}"
                                           class="form-control regStepOne" id="description_ru" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="description_uz">Описание (UZ)</label>
                                    <input required="required" type="text" name="description_uz"
                                           value="{{$data->description_uz}}"
                                           class="form-control regStepOne" id="description_uz" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="description_en">Описание (EN)</label>
                                    <input required="required" type="text" name="description_en"
                                           value="{{$data->description_en}}"
                                           class="form-control regStepOne" id="description_en" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="seats_count">Кол-во мест (шт)</label>
                                    <input required="required" type="number" min="1" name="seats_count"
                                           value="{{$data->seats_count}}"
                                           class="form-control regStepOne" id="seats_count" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="deposit">Мин заказ (UZS)</label>
                                    <input required="required" type="number" min="0" name="deposit"
                                           value="{{$data->deposit}}"
                                           class="form-control regStepOne" id="deposit" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="prepaid_percentage">Процент предоплаты для ресторана %</label>
                                    <input required="required" type="number" min="0" name="prepaid_percentage"
                                           value="{{$data->prepaid_percentage}}"
                                           class="form-control regStepOne" id="prepaid_percentage" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="percentage">Процент системы %</label>
                                    <input required="required" type="number" min="0" name="percentage"
                                           value="{{$data->percentage}}"
                                           class="form-control regStepOne" id="percentage" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="fee_per_seat">Цена системы за место (UZS)</label>
                                    <input required="required" type="number" min="0" name="fee_per_seat"
                                           value="{{$data->fee_per_seat}}"
                                           class="form-control regStepOne" id="fee_per_seat" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block pb-3 jv_map_register">
                                <div class="input">
                                    <label>Локация:</label>
                                    <div id="map" style="height: 400px; width: 100%;"></div>
                                    <input id="long" value="{{$data->long}}" name="long" class="form-control"/>
                                    <input id="lat" value="{{$data->lat}}" name="lat" class="form-control"/>
                                </div>
                            </div>
                            <div class="input-block">
                                @foreach($workdays as $key => $info)
                                    <div class="col-lg-12 p-0 d-flex align-items-center">
                                        <div style="min-width: 140px">
                                            <label class="form-check form-check-inline">
                                                <input class="form-check-input schedule-handler" name="closed[]"
                                                       value="{{$week_map[$key]}}" @if(!$info->closed) checked="checked"
                                                       @endif type="checkbox">
                                                <span class="form-check-label">{{$week_map[$key]}}</span>
                                            </label>
                                        </div>
                                        <input type="time" class="time" name="open_time[]" style="width: 80px;"
                                               value="{{$info['open_time']}}"/>
                                        <div style="width: 10px;"></div>
                                        <input type="time" class="time" name="close_time[]" style="width: 80px;"
                                               value="{{$info['close_time']}}"/>
                                    </div>
                                @endforeach
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label>Загрузите для обновления Logo 120x120:</label>
                                    <input type="file" name="logo[]">
                                    <div class="d-flex justify-content-between mt-5">
                                        @foreach($images as $image)
                                            <img src="{{asset($image)}}?time={{microtime(true)}}" alt="image"
                                                 width="150"/>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="white-block mb-30">
                        <div class="head">
                            <h3>Загрузите для обновления Баннер 640x360</h3>
                        </div>
                        <div class="card-header-after">
                            <p class="pt-3"></p>
                            <div class="uploader">
                                <ul class="jFiler-items-list jFiler-items-grid">
                                    @foreach($banner as $image)
                                        <li class="gallryUploadBlock_item photo-thumbler d-inline-block"
                                            data-image="{{basename($image)}}">
                                            <div class="jFiler-item-thumb-image">
                                                <img src="{{asset($image)}}" alt="image">
                                            </div>
                                            <div class="removeItem">
                                            <span class="deletePhoto" data-image="{{basename($image)}}">
                                                <i class="fe fe-minus"></i>
                                            </span>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                                <input type="file" name="image[]" class="filer_input3" multiple="multiple">
                            </div>
                        </div>
                    </div>
                    <div class="button-block">
                        <button type="submit" class="continue-btn">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
@section('style')
    <link rel="stylesheet" href="{{asset('backend/css/chosen.css')}}">
@endsection
@section('script')
    <script async defer src="https://maps.googleapis.com/maps/api/js?callback=initMap"></script>
    <script src="{{ asset('backend/js/vendors/chosen.js') }}"></script>
    <script>
        let latitude = $('#lat');
        let longitude = $('#long');
        $(document).ready(function () {
            $(".filer_input3").filer({
                limit: null,
                maxSize: null,
                extensions: null,
                changeInput: '<a class="btnAddImageToGallery galleryAddElement"><i class="fe fe-plus"></i></a>',
                showThumbs: true,
                theme: "dragdropbox",
                templates: {
                    box: '<ul class="jFiler-items-list buttonAdder jFiler-items-grid"></ul>',
                    item: `<li class="gallryUploadBlock_item jFiler-item">
                    {{'{{fi-image}'.'}'}}
                    <div class="removeItem"  ><span><i class="fe fe-minus"></i></span></div>
                    </li>`,
                    progressBar: '<div class="bar"></div>',
                    itemAppendToEnd: true,
                    canvasImage: true,
                    removeConfirmation: false,
                    _selectors: {
                        list: '.jFiler-items-list',
                        item: '.jFiler-item',
                        progressBar: '.bar',
                        remove: '.fe.fe-minus'
                    }
                },
                dragDrop: {
                    dragEnter: null,
                    dragLeave: null,
                    drop: null,
                    dragContainer: null,
                },
                files: null,
                addMore: true,
                allowDuplicates: false,
                clipBoardPaste: true,
                excludeName: null,
                beforeRender: null,
                afterRender: null,
                beforeShow: null,
                beforeSelect: null,
                itemAppendToEnd: true,
                onSelect: null,
                afterShow: function (jqEl, htmlEl, parentEl, itemEl) {
                    $('.galleryAddElement').hide()
                    $('.cloneGalleryAddElement').remove()
                    $('.buttonAdder').append('<a class="btnAddImageToGallery cloneGalleryAddElement"><i class="fe fe-plus"></i></a>')
                },
                onRemove: function (itemEl, file, id, listEl, boxEl, newInputEl, inputEl) {
                    let filerKit = inputEl.prop("jFiler"),
                        file_name = filerKit.files_list[id].name;
                    if (filerKit.files_list.length == 1) {
                        $('.galleryAddElement').show()
                    }
                },
                onEmpty: null,
                options: null,
            });
            $('.uploader').on('click', '.cloneGalleryAddElement', function (e) {
                $('.galleryAddElement').trigger('click');
            });
            $('.deletePhoto').click((e) => {
                let removeImage = $(e.target);
                let imageData = removeImage.data('image');
                if (confirm('Вы уверены?')) {
                    let deleteItem = $.get("{{route('restaurant.remove',$data->id)}}", {file_name: imageData});
                    deleteItem.done(() => {
                        removeImage.parent().parent().remove();
                    });
                }
            });
        });
        function initMap() {
            let latlng = new google.maps.LatLng({{$data->lat}}, {{$data->long}});
            let map = new google.maps.Map(document.getElementById('map'), {
                center: latlng,
                zoom: 12,
                animation: google.maps.Animation.BOUNCE
            });
            let marker = new google.maps.Marker({
                position: latlng,
                map: map,
                draggable: true
            });
            let lat, long;
            google.maps.event.addListener(marker, 'dragend', function (event) {
                lat = this.getPosition().lat().toFixed(7);
                long = this.getPosition().lng().toFixed(7);
                // console.log(lat + 'lat' + long + 'lng')
                latitude.val(lat);
                longitude.val(long);
            });
        }

        $(function () {
            $('.chosen-select').chosen();
        });
    </script>
@endsection
