@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <h2 class="blue-title d-flex align-items-center">Параметр Еды : <a
                    href="{{route('food.index',$food->restaurant_id)}}"
                    class="ml-2 badge badge-dark">{{$food->name_ru}}</a></h2>
            <div class="row">
                <div class="form">
                    <div class="white-block">
                        <ul class="list-block">
                            @foreach ($data as $inner)
                                <li class="list-group-item">
                                    <div class="d-flex justify-content-between align-items-center"
                                         style="min-height: 50px">
                                        <span>
                                            <i class="fe fe-list"></i> {{ $inner->name_ru }}
                                        </span>
                                        <div class="d-flex justify-content-center align-items-center">
                                            <a href="{{ route('params.edit',$inner->id) }}"
                                               class="btn btn-outline-primary">Изменить</a>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                            <a class="add-list-btn" href="{{ route('params.create',$food->id) }}">
                                <i class="fe fe-plus-circle"></i>
                                Добавить
                            </a>
                        </ul>
                    </div>
                    <div class="button-block">
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
@endsection
