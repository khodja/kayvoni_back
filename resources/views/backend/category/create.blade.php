@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <form action="{{ route('category.store') }}" method="POST" enctype="multipart/form-data"
                      autocomplete="off" class="form">
                    @csrf
                    <div class="white-block mb-30">
                        <div class="head">
                            <h3>Добавить Категорию</h3>
                        </div>
                        <div class="content">
                            <div class="input-block">
                                <div class="input">
                                    <label for="name_ru">Название (RU)</label>
                                    <input required="required" type="text" name="name_ru" value="{{old('name_ru')}}"
                                           class="form-control regStepOne" id="name_ru" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="name_uz">Название (UZ)</label>
                                    <input required="required" type="text" name="name_uz" value="{{old('name_uz')}}"
                                           class="form-control regStepOne" id="name_uz" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="name_en">Название (EN)</label>
                                    <input required="required" type="text" name="name_en" value="{{old('name_en')}}"
                                           class="form-control regStepOne" id="name_en" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label>Фото 120x120:</label>
                                    <input type="file" required="required" name="image[]">
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="color">Выберите цвет: </label> <br>
                                    <input required="required" class="jscolor" id="color" name="color"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="button-block">
                        <button type="submit" class="continue-btn">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{asset('backend/js/vendors/jscolor.js')}}"></script>
@endsection
