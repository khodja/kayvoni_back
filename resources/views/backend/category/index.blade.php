@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <h2 class="blue-title">Категории</h2>
            <div class="row">
                <div class="form">
                    <div class="white-block">
                        <ul class="list-block">
                            @foreach ($data as $category)
                                <li class="list-group-item">
                                    <div class="d-flex justify-content-between align-items-center"
                                         style="min-height: 50px">
                                <span>
                                    <i class="fe fe-list"></i> {{ $category->name_ru }}
                                </span>
                                        <div class="d-flex justify-content-center align-items-center">
                                            <a href="{{ route('category.edit',$category->id) }}"
                                               class="btn btn-outline-primary">Изменить</a>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                            <a class="add-list-btn" href="{{ route('category.create') }}">
                                <i class="fe fe-plus-circle"></i>
                                Добавить категорию
                            </a>
                        </ul>
                    </div>
                    <div class="button-block">
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
