@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <form action="{{ route('food.update',$data->id) }}" method="POST"
                      enctype="multipart/form-data" autocomplete="off" class="form">
                    @csrf
                    @method('PUT')
                    <div class="white-block mb-30">
                        <div class="head">
                            <h3>Изменить</h3>
                        </div>
                        <div class="content">
                            <div class="input-block">
                                <div class="input">
                                    <label for="name_ru">Название (RU)</label>
                                    <input required="required" type="text" name="name_ru" value="{{ $data->name_ru }}"
                                           class="form-control regStepOne" id="name_ru" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="name_uz">Название (UZ)</label>
                                    <input required="required" type="text" name="name_uz" value="{{ $data->name_uz }}"
                                           class="form-control regStepOne" id="name_uz" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="name_en">Название (EN)</label>
                                    <input required="required" type="text" name="name_en" value="{{ $data->name_en }}"
                                           class="form-control regStepOne" id="name_en" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="description_ru">Описание (RU)</label>
                                    <input required="required" type="text" name="description_ru"
                                           value="{{$data->description_ru}}"
                                           class="form-control regStepOne" id="description_ru" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="description_uz">Описание (UZ)</label>
                                    <input required="required" type="text" name="description_uz"
                                           value="{{$data->description_uz}}"
                                           class="form-control regStepOne" id="description_uz" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="description_en">Описание (EN)</label>
                                    <input required="required" type="text" name="description_en"
                                           value="{{$data->description_en}}"
                                           class="form-control regStepOne" id="description_en" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="category_id">Категория</label>
                                    <select name="category_id" id="category_id" class="form-control"
                                            required="required">
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}"
                                                    @if($category->id == $data->category_id) selected @endif>{{$category->name_ru}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label>Загрузите для обновления фотографии {{$photo_size['width']}}
                                        x{{$photo_size['height']}}</label>
                                    <input type="file" name="image[]" class="image">
                                    <div class="d-flex justify-content-start mt-5">
                                        @foreach($images as $image)
                                            <img src="{{asset($image)}}?time={{microtime(true)}}" alt="image"
                                                 width="150"/>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="button-block">
                        <button type="submit" class="continue-btn">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('script')
@endsection
