@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <h2 class="blue-title d-flex align-items-center">Еда <a href="{{route('restaurant.index',$restaurant->id)}}"
                    class="ml-2 badge badge-dark">{{$restaurant->name}}</a></h2>
            <div class="row">
                <div class="form">
                    <div class="white-block">
                        <ul class="list-block">
                            @foreach ($data as $food)
                                <li class="list-group-item">
                                    <div class="d-flex justify-content-between align-items-center"
                                         style="min-height: 50px">
                                <span>
                                    <i class="fe fe-list"></i> {{ $food->name_ru }}
                                </span>
                                        <div class="d-flex justify-content-center align-items-center">
                                            <label class="custom-switch mr-1">
                                                <span class="custom-switch-description mr-1">OFF/ON</span>
                                                <input type="checkbox" name="custom-switch-checkbox"
                                                       class="custom-switch-input"
                                                       @if($food->enabled) checked="checked" @endif>
                                                <span class="custom-switch-indicator actioner"
                                                      data-href="{{route('food.switch',$food->id)}}"></span>
                                            </label>
                                            <a href="{{ route('params.index',$food->id) }}"
                                               class="btn btn-outline-primary mr-2">Параметры выбора</a>
                                            <a href="{{ route('food.edit',$food->id) }}"
                                               class="btn btn-outline-primary">Изменить</a>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                            <a class="add-list-btn" href="{{ route('food.create',$restaurant->id) }}">
                                <i class="fe fe-plus-circle"></i>
                                Добавить
                            </a>
                        </ul>
                    </div>
                    <div class="button-block">
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $('.actioner').on('click', function () {
            let url = $(this).data('href');
            $.get(url);
        })
    </script>
@endsection
