@extends('layouts.backend')

@section('content')
    <div class="container-fluid pt-4">
        <div class="d-flex justify-content-between">
            <div>
                <a href="{{ route('order.index') }}" class="btn btn-primary">Все</a>
                <a href="{{ route('order.index','filter=month') }}" class="btn btn-primary">За месяц</a>
                <a href="{{ route('order.index','filter=week') }}" class="btn btn-primary">За неделю</a>
                <a href="{{ route('order.index','filter=day') }}" class="btn btn-primary">За сегодня</a>
            </div>
            <form action="{{ route('order.index')  }}" class="mt-2 align-items-center">
                От <input type="date" name="from" @if (!empty($from))value="{{ $from }}" @endif required>
                До <input type="date" name="to" @if (!empty($to))value="{{$to}}" @endif required>
                <button type="submit" class="btn btn-outline-success">За Период</button>
            </form>
        </div>
        <div class="card my-3 my-md-5">
            <div class="card-header justify-content-between">
                <h3 class="card-title">
                    Заказы
                    @if( !empty($filter) )
                        за
                        {{ $filter == 'month' ? 'Месяц' : ''  }}
                        {{ $filter == 'week' ? 'Неделю' : ''  }}
                        {{ $filter == 'day' ? 'День' : ''  }}
                    @endif
                </h3>
                @if( empty($id) )
                    <div>
                        {{--                        <a href="{{ route('order.import') }}" class="btn btn-info">Excel</a>--}}
                        {{--                        <a href="{{ route('review.show') }}" class="btn btn-primary">Все Отзывы</a>--}}
                    </div>
                @endif
            </div>
            <div class="table-responsive">
                <table class="table card-table table-vcenter text-nowrap">
                    <thead>
                    <tr>
                        <th class="w-1">No</th>
                        <th>Дата</th>
                        <th class="w-1">Пользователь</th>
                        <th>Ресторан</th>
                        <th>Статус</th>
                        <th>Сумма Kayvoni</th>
                        <th>Сумма ресторана</th>
                        <th>Сумма за место</th>
                        <th>Общая сумма</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $datas)
                        <tr
                            @if($datas->status == 1) class="table-primary" @endif
                            @if($datas->status == 2) class="table-success" @endif
                            @if($datas->status == 3) class="table-active" @endif
                            @if($datas->status == -3) class="table-primary" @endif
                            @if($datas->status == -2) class="table-warning" @endif
                        >
                            <td>#{{ $datas->id }}</td>
                            <td>{{ \Carbon\Carbon::parse($datas->reserve_time)->isoFormat('DD/MM/YY - HH:mm') }}</td>
                            <td>{{ $datas->user->phone }}</td>
                            <td>{{ $datas->restaurant->name }}</td>
                            {{--                                        <td>{{ $datas->shipping_time !== '0' ? $datas->shipping_time : 'Не указано'  }}</td>--}}
                            <td class="time">
                                {{$datas->showStatus()}}
                            </td>
                            <td>{{ $datas->system_price}} UZS</td>
                            <td>{{ $datas->restaurant_price }} UZS</td>
                            <td>{{ $datas->fee_per_seat * $datas->seats_count }} UZS</td>
                            <td>{{ $datas->real_price }} UZS</td>
                            <td class="text-center">
                                <a href="{{ route('order.show', $datas->id)  }}"
                                   class="btn btn-secondary btn-sm">
                                    Подробности
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        {{ $data->appends($_GET)->links() }}
    </div>
    </div>
@endsection
@section('script')
    <script>

    </script>
@endsection
