@extends('layouts.backend')

@section('content')
    <div class="my-3 my-md-5">
        <div class="container">
            <div class="row row-cards">
                <div class="col-lg-3">
                    <div class="row">
                        <div class="col-md-6 col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="mb-4 text-center">
                                        <img src="{{ $data->restaurant->logo  }}" class="img-fluid">
                                    </div>
                                    <h4 class="card-title"><a
                                            href="javascript:void(0)">{{ $data->restaurant->name  }}</a></h4>
                                    <div class="card-title">
                                        Статус: {{$data->showStatus()}}
                                    </div>
                                    <div class="card-subtitle">
                                        <a href="http://maps.google.com/?q={{$data->restaurant->lat.','.$data->restaurant->long}}"
                                           target="_blank">
                                            Локация ресторана
                                        </a>
                                    </div>
                                    <div class="card-subtitle">
                                        Комментарий к заказу:
                                        {{ $data->comment ? $data->comment :'Нет'}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Пользователь : {{ $data->user->name  }}</h4>
                                    <div class="card-subtitle">
                                        Телефон: {{ $data->user->phone  }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="card">
                        <table class="table card-table table-vcenter">
                            <tbody>
                            @foreach($data->order_item as $datas)
                                <tr>
                                    <td><img src="{{ $datas->food->image }}" alt="image" width="100"/></td>
                                    <td class="text-center">
                                        {{  $datas->food->name_ru }} -
                                        <strong>{{  $datas->total_price / $datas->count }}
                                            сум</strong>
                                    </td>
                                    <td class="text-right text-muted d-md-table-cell text-nowrap">{{  $datas->count }}
                                        шт
                                    </td>
                                    <td class="text-right">
                                        <strong>{{  $datas->total_price }}
                                            сум</strong>
                                    </td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="4" class="text-right">Сумма для ресторана :
                                    <strong>{{ $data->restaurant_price  }} сум ({{$data->restaurant_percentage}}%)</strong></td>
                            </tr>
                            <tr>
                                <td colspan="4" class="text-right">Сумма для Системы :
                                    <strong>{{$data->seats_count * $data->fee_per_seat}}({{$data->fee_per_seat}} за стол * {{$data->seats_count}} шт) + {{ $data->system_price  }} сум ({{$data->system_percentage}}%)</strong></td>
                            </tr>
                            <tr>
                                <td colspan="4" class="text-right">Стоимость заказа :
                                    <strong>{{ $data->total_price  }} сум</strong></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
