@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <form action="{{ route('promo.store',$restaurant->id) }}" method="POST" enctype="multipart/form-data"
                      autocomplete="off" class="form">
                    @csrf
                    <div class="white-block mb-30">
                        <div class="head">
                            <h3>Добавить</h3>
                        </div>
                        <div class="content">
                            <div class="input-block">
                                <div class="input">
                                    <label for="name_ru">Название (RU)</label>
                                    <input required="required" type="text" name="name_ru" value="{{old('name_ru')}}"
                                           class="form-control regStepOne" id="name_ru" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="name_uz">Название (UZ)</label>
                                    <input required="required" type="text" name="name_uz" value="{{old('name_uz')}}"
                                           class="form-control regStepOne" id="name_uz" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="name_en">Название (EN)</label>
                                    <input required="required" type="text" name="name_en" value="{{old('name_en')}}"
                                           class="form-control regStepOne" id="name_en" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="description_ru">Описание (RU)</label>
                                    <input required="required" type="text" name="description_ru"
                                           value="{{old('description_ru')}}"
                                           class="form-control regStepOne" id="description_ru" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="description_uz">Описание (UZ)</label>
                                    <input required="required" type="text" name="description_uz"
                                           value="{{old('description_uz')}}"
                                           class="form-control regStepOne" id="description_uz" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="description_en">Описание (EN)</label>
                                    <input required="required" type="text" name="description_en"
                                           value="{{old('description_en')}}"
                                           class="form-control regStepOne" id="description_en" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="food">Выберите меню</label>
                                    <select multiple name="food_id[]" required id="food"
                                            class="chosen-select form-control">
                                        @foreach( $food as $datas )
                                            <option value="{{ $datas->id }}">{{ $datas->name_ru }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="start_date">Начало акции</label>
                                    <input required="required" type="date" name="start_date"
                                           value="{{old('start_date')}}"
                                           class="form-control regStepOne" id="start_date" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="end_date">Конец акции</label>
                                    <input required="required" type="date" name="end_date"
                                           value="{{old('end_date')}}"
                                           class="form-control regStepOne" id="end_date" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label>Фото {{$photo_size['width']}}x{{$photo_size['height']}}:</label>
                                    <input type="file" required="required" name="image[]">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="button-block">
                        <button type="submit" class="continue-btn">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
@section('style')
    <link rel="stylesheet" href="{{asset('backend/css/chosen.css')}}">
@endsection
@section('script')
    <script src="{{ asset('backend/js/vendors/chosen.js') }}"></script>
    <script>
        $(function () {
            $('.chosen-select').chosen();
        });
    </script>
@endsection
