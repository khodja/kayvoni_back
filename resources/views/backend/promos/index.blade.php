@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <h2 class="blue-title d-flex align-items-center">Акции <a
                    href="{{route('restaurant.index',$restaurant->id)}}"
                    class="ml-2 badge badge-dark">{{$restaurant->name}}</a></h2>
            <div class="row">
                <div class="form">
                    <div class="white-block">
                        <ul class="list-block">
                            @foreach ($data as $promo)
                                <li class="list-group-item">
                                    <div class="d-flex justify-content-between align-items-center"
                                         style="min-height: 50px">
                                        <span>
                                            {{ $promo->name_ru }}
                                        </span>
                                        <div class="d-flex justify-content-center align-items-center">
                                            <span class="badge badge-dark mr-2">
                                                {{ $promo->isActive() ? 'Активен' : 'Не активен' }}
                                            </span>
                                            <a href="{{ route('promo.edit',$promo->id) }}"
                                               class="btn btn-outline-primary">Изменить</a>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                            <a class="add-list-btn" href="{{ route('promo.create',$restaurant->id) }}">
                                <i class="fe fe-plus-circle"></i>
                                Добавить
                            </a>
                        </ul>
                    </div>
                    <div class="button-block">
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $('.actioner').on('click', function () {
            let url = $(this).data('href');
            $.get(url);
        })
    </script>
@endsection
