@extends('layouts.backend')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <div>
                            Статистика
                        </div>
                        <div>
                            <a href="{{route('home.refresh')}}" class="btn btn-warning">Optimize cache</a>
                            <a href="{{route('home.clearCache')}}" class="btn btn-warning">Clear cache</a>
                            <a href="{{route('home.clearRoute')}}" class="btn btn-warning">ClearRoute</a>
                            <a href="{{route('home.clearView')}}" class="btn btn-warning">Clear View</a>
                            <a href="{{route('home.clearConfig')}}" class="btn btn-warning">Clear Config</a>
                            <a href="{{route('home.clearOptimize')}}" class="btn btn-warning">Clear Optimize</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-12">
                <div class="row">
                    <div class="col-sm-6 col-lg-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-value float-right text-blue">{{$restaurants->count()}}</div>
                                <h3 class="mb-1">Кол-во ресторанов</h3>
                                <div class="text-muted">Загруженных</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-value float-right text-blue">{{$foods->count()}}</div>
                                <h3 class="mb-1">Кол-во блюд</h3>
                                <div class="text-muted">Загруженных</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-value float-right text-blue">{{$users->count()}}</div>
                                <h3 class="mb-1">Кол-во юзеров</h3>
                                <div class="text-muted">Активных</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-value float-right text-blue">{{$orders->count()}}</div>
                                <h3 class="mb-1">Кол-во заказов</h3>
                                <div class="text-muted">Закрытых</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6">
                        <div class="card">
                            <div class="card-header">
                                <h2 class="card-title">Самые популярные блюдо</h2>
                            </div>
                            <table class="table card-table">
                                <tbody>
                                @foreach($foods as $key => $food)
                                    <tr>
                                        <td>{{$food->name_ru}}</td>
                                        <td class="text-right">
                                            <span class="badge badge-default">{{$food->order_items_count}} шт</span>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="card">
                            <div class="card-body p-3 text-center">
                                <div class="text-right text-green">
                                    {{$orders->where('created_at', \Carbon\Carbon::today())->count()}} шт
                                    <i class="fe fe-chevron-up"></i>
                                </div>
                                <div
                                    class="h1 m-0">{{$orders->where('created_at', \Carbon\Carbon::today())->sum('restaurant_price')}}
                                    UZS
                                </div>
                                <div class="text-muted mb-4">Профит на сегодня (Ресторана)</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body p-3 text-center">
                                <div class="text-right text-green">
                                    <i class="fe fe-chevron-up"></i>
                                </div>
                                <div
                                    class="h1 m-0">{{$orders->where('created_at', \Carbon\Carbon::today())->sum('system_price') + $orders->where('created_at', \Carbon\Carbon::today())->sum('seats_price')}}
                                    UZS
                                </div>
                                <div class="text-muted mb-4">Профит на сегодня (Системы)</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="card">
                            <div class="card-body p-3 text-center">
                                <div class="text-right text-green">
                                    {{$orders->count()}} шт
                                    <i class="fe fe-chevron-up"></i>
                                </div>
                                <div class="h1 m-0">{{$orders->sum('restaurant_price')}} UZS</div>
                                <div class="text-muted mb-4">Профит за все время (Ресторана)</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body p-3 text-center">
                                <div class="text-right text-green">
                                    <i class="fe fe-chevron-up"></i>
                                </div>
                                <div class="h1 m-0">{{$orders->sum('system_price') + $orders->sum('seats_price')}}UZS
                                </div>
                                <div class="text-muted mb-4">Профит за все время (Системы)</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
