@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <h2 class="blue-title">Регион <span
                    class="badge badge-dark">{{ isset($parent) ? $parent->name_ru : ''}}</span></h2>
            <div class="row">
                <div class="form">
                    <div class="white-block">
                        <ul class="list-block">
                            @foreach($data as $datas)
                                <li class="list-group-item">
                                    <div class="d-flex justify-content-between align-items-center"
                                         style="min-height: 50px">
                                        <span><i class="fe fe-list"></i> {{ $datas->name_ru }}</span>
                                        <div class="d-flex justify-content-center align-items-center">
                                            <a href="{{ route('city.edit',$datas->id) }}" style="margin-right: 10px;" class="btn btn-outline-primary">Изменить</a>
                                            <a href="{{ route('city.show' , $datas->id) }}"
                                               class="btn btn-outline-success"><i class="fe fe-arrow-right"></i></a>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                            <form action="{{ route('city.create') }}" class="d-inline-block">
                                @isset($parent)
                                    <input type="hidden" name="parent_id" value="{{$parent->id}}">
                                @endif
                                <button class="add-list-btn" style="border: none;">
                                    <i class="fe fe-plus-circle"></i>
                                    Добавить
                                </button>
                            </form>
                        </ul>
                    </div>
                    <div class="button-block">

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
