@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <form class="form" action="{{ route('city.store') }}" method="POST"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="white-block mb-30">
                        <div class="head"><h3>Добавить Регион</h3></div>
                        <div class="content">
                            <div class="input-block">
                                <div class="input">
                                    <label for="name-ru">Название Ru</label>
                                    <input required="required" name="name_ru" type="text" class="form-control"
                                           id="name-ru" placeholder="Введите название"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="name-uz">Название Uz</label>
                                    <input required="required" name="name_uz" type="text" class="form-control"
                                           id="name-uz" placeholder="Введите название"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="name-kr">Название EN</label>
                                    <input required="required" name="name_en" type="text" class="form-control"
                                           id="name-kr" placeholder="Введите название"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="custom_id">Привязанный регион:</label>
                                    <select id="custom_id" name="parent_id" class="form-control custom-select">
                                        <option value="0">Нет привязанности</option>
                                        @foreach( $data as $datas )
                                            <option
                                                value="{{ $datas->id }}" {{ request()->has('parent_id') ? (request()->get('parent_id') == $datas->id ? 'selected' : '') : '' }}>{{ $datas->name_ru }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="button-block">
                        <button type="submit" class="continue-btn">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('script')

@endsection
