@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <form class="form" action="{{ route('city.update',$data->id) }}" method="POST"
                      enctype="multipart/form-data" autocomplete="off">
                    @csrf
                    @method('PUT')
                    <div class="white-block mb-30">
                        <div class="head">
                            <h3>Изменить</h3>
                        </div>
                        <div class="content">
                            <div class="input-block">
                                <div class="input">
                                    <label for="name_ru">Название (RU)</label>
                                    <input required="required" type="text" name="name_ru" value="{{ $data->name_ru }}"
                                           class="form-control regStepOne" id="name_ru" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="name_uz">Название (UZ)</label>
                                    <input required="required" type="text" name="name_uz" value="{{ $data->name_uz }}"
                                           class="form-control regStepOne" id="name_uz" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="name_en">Название (EN)</label>
                                    <input required="required" type="text" name="name_en" value="{{ $data->name_en }}"
                                           class="form-control regStepOne" id="name_en" placeholder=""/>
                                </div>
                            </div>
                            @if($data->parent_id != null)
                                <div class="input-block">
                                    <div class="input">
                                        <label>Привязанный регион:</label>
                                        <select name="parent_id" class="form-control custom-select">
                                            <option value="0">Отвязать от всех</option>
                                            @foreach( $categories as $datas )
                                                <option value="{{ $datas->id }}"
                                                        @if($data->parent_id == $datas->id) selected @endif>{{ $datas->name_ru }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="button-block">
                        <button type="submit" class="continue-btn">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('script')
@endsection
