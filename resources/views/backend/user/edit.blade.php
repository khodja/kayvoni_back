@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <form action="{{ route('manager.update',$data->id) }}" method="POST" enctype="multipart/form-data"
                      autocomplete="off" class="form">
                    @csrf
                    @method('PUT')
                    <div class="white-block mb-30">
                        <div class="head">
                            <h3>Добавить</h3>
                        </div>
                        <div class="content">
                            <div class="input-block">
                                <div class="input">
                                    <label for="name">Имя</label>
                                    <input required="required" type="text" name="name" value="{{$data->name}}"
                                           class="form-control regStepOne" id="name" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="phone">Телефон</label>
                                    <input required="required" type="tel" name="phone" value="{{$data->phone}}"
                                           class="form-control regStepOne" id="phone" placeholder="990008991"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="restaurant_id">Ресторан</label>
                                    <select class="form-control" required="required" id="restaurant_id"
                                            name="restaurant_id">
                                        @foreach($restaurants as $rest)
                                            <option value="{{$rest->id}}"
                                                    @if($data->restaurant_id == $rest->id) selected @endif>{{$rest->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="password">Пароль (для обновления введите новый)</label>
                                    <input type="text" name="password"
                                           class="form-control regStepOne" id="password"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="button-block">
                        <button type="submit" class="continue-btn">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
@section('script')
@endsection
