@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <h2 class="blue-title">{{$title ?? 'Пользователи'}}</h2>
            <div class="row">
                <div class="form">
                    <div class="white-block">
                        <ul class="list-block">
                            @foreach ($data as $user)
                                <li class="item">
                                    <div class="left-block">
                                        {{ $user->name }} {{ $user->phone }}
                                    </div>
                                    @isset($title)
                                        <div class="right-block">
                                            <div class="mr-2 badge badge-dark">
                                                Restaurant : {{$user->restaurant->name}}
                                            </div>
                                            <a href="{{ route('manager.edit',$user->id) }}"
                                               class="btn btn-outline-primary mr-1">Изменить</a>
                                        </div>
                                    @endisset
                                </li>
                            @endforeach
                            @isset($title)
                                <a class="add-list-btn" href="{{ route('manager.create') }}">
                                    <i class="fe fe-plus-circle"></i>
                                    Добавить
                                </a>
                            @endisset
                        </ul>
                    </div>
                </div>
                <div class="button-block"></div>
            </div>
        </div>
    </section>
@endsection
