@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <form action="{{ route('manager.store') }}" method="POST" enctype="multipart/form-data"
                      autocomplete="off" class="form">
                    @csrf
                    <div class="white-block mb-30">
                        <div class="head">
                            <h3>Добавить</h3>
                        </div>
                        <div class="content">
                            <div class="input-block">
                                <div class="input">
                                    <label for="name">Имя</label>
                                    <input required="required" type="text" name="name" value="{{old('name')}}"
                                           class="form-control regStepOne" id="name" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="phone">Телефон</label>
                                    <input required="required" type="tel" name="phone" value="{{old('phone')}}"
                                           class="form-control regStepOne" id="phone" placeholder="990008991"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="restaurant_id">Ресторан</label>
                                    <select class="form-control" required="required" id="restaurant_id"
                                            name="restaurant_id">
                                        <option disabled selected>Не выбрано</option>
                                        @foreach($restaurants as $rest)
                                            <option value="{{$rest->id}}">{{$rest->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="password">Пароль</label>
                                    <input required="required" type="text" name="password" value="{{old('password')}}"
                                           class="form-control regStepOne" id="password"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="button-block">
                        <button type="submit" class="continue-btn">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
@section('script')
@endsection
