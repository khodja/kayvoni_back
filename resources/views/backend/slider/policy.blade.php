@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <div class="d-flex align-items-center justify-content-end">
                <a href="#ru" class="btn btn-primary">RU</a>
                <a href="#uz" class="btn btn-primary ml-2 mr-2">UZ</a>
                <a href="#en" class="btn btn-primary">EN</a>
            </div>
            <p id="ru" class="mt-2"><strong>Пользовательское соглашение</strong></p>
            <p><strong>об использовании системы "Kayvoni"</strong></p>
            <p>Настоящее пользовательское соглашение (далее именуемое "Соглашение") регулирует отношения между
                ООО "UNITED RESERVATION SYSTEM", (далее именуемая как - "Администрация"), и пользователем сети Интернет
                (далее именуемый "Пользователь") по использованию системы Kayvoni (далее именуемая
                "Система Kayvoni", "Система").</p>
            <p>Настоящее Соглашение в соответствии с Гражданским кодексом Республики Узбекистан является публичной
                офертой, адресованной любым третьим лицам, использующим Систему Kayvoni. Перед использованием Системы
                Kayvoni внимательно ознакомьтесь с условиями настоящего Соглашения. Если Вы не согласны с условиями
                настоящего Соглашения, Вы не можете использовать Систему. Совершение действий по использованию Системы,
                указанных в настоящем Соглашении, означает Ваше полное и безоговорочное согласие (далее
                "Акцепт") со всеми условиями настоящего Соглашения.</p>
            <p><strong>ТЕРМИНЫ И ОПРЕДЕЛЕНИЯ</strong></p>
            <p>Система Kayvoni (Система) система мобильных платежей, а также бронирования, доступная в
                виде Мобильного приложения и веб-версии, позволяющая Пользователям осуществлять Мобильные
                платежи в Ресторанах, также осуществлять поиск Ресторанов, и онлайн Бронирование мест в
                Ресторанах, а также, использовать иные доступные функции Системы.
            </p>
            <p>Мобильное приложение "Kayvoni" - программа для ЭВМ в соответствии с Гражданского кодекса
                Республика Узбекистан, доступная для установки на мобильные устройства (мобильный телефон,
                смартфон, планшет и др.), использующие операционную систему "iOS"/ "Android", позволяющая
                Пользователям использовать Систему.
            </p>
            <p>Администрация ООО "United Reservation System", ИНН 307905283, зарегистрированное по
                адресу: Кичик халка юли, 89a, которому принадлежит исключительное право на Систему Kayvoni и
                Мобильные приложения.
            </p>
            <p>Пользователь (Гость) физическое лицо, зарегистрировавшееся в Системе для использования
                Системы в соответствии с настоящим Соглашением.
            </p>
            <p>Ресторан (Заведение) юридическое лицо или индивидуальный предприниматель, являющийся
                владельцем ресторана, кафе или иного заведения, предоставляющего третьим лицам товары и/или
                услуги в сфере общественного питания, информация о котором размещена в Системе.
            </p>
            <p>Регистрация в Системе - процедура, посредством которой Пользователь при первом входе в Мобильное
                приложение выражает свое полное и безоговорочное согласие с условиями настоящего Соглашения и
                всеми применимыми к Системе документами и получает доступ к использованию Системы при помощи
                Номера телефона и ввода Кода подтверждения. При этом ввод Кода подтверждения считается
                необходимым и достаточным для доступа к Системе и согласие с Указанными правилами системы
                (Пользовательским соглашением).
            </p>
            <p>Номер телефона - телефонный номер, назначенный Пользователю оператором сотовой связи в момент
                подключения Пользователя к сети оператора сотовой связи, однозначно определяющий лицо,
                заключившее с оператором сотовой связи договор о предоставлении услуг связи, выступающий
                идентификатором Пользователя в Системе. Номер телефона является уникальным именем Пользователя
                (логином) в Системе, которое присваивается ему в момент Регистрации в Системе.
            </p>
            <p>Код подтверждения - уникальная последовательность цифр (пароль), инициируемая Пользователем и
                генерируемая Администрацией, направляемая Пользователю Администрацией посредством SMS-сообщения
                на Номер телефона Пользователя, указанный им при Регистрации в Системе, в целях идентификации
                Пользователя (пароль действителен в течение 1-ой минуты с момента отправки Администрацией
                SMS-сообщения).
            </p>
            <p>Бронирование функция Системы, позволяющая Пользователям при пользовании Мобильным
                приложением Kayvoni осуществлять поиск Ресторанов и заказывать места в Ресторанах через форму
                бронирования, размещенную в Системе.
            </p>
            <p>Акцепт - это положительный ответ на предложение другой стороны заключить договор.</p>
            <p>Оферта -предложение о заключении сделки, в котором изложены существенные
                условия договора, адресованное определённому лицу, ограниченному или неограниченному
                кругу лиц.
            </p>
            <p><strong>ПРЕДМЕТ СОГЛАШЕНИЯ</strong></p>
            <p>По настоящему Соглашению Администрация предоставляет Пользователю доступ к функциональным
                возможностям (функциям) Системы Kayvoni, позволяющим Пользователю, включая, но не ограничиваясь,
                осуществлять Мобильные платежи в Ресторанах, а также осуществлять поиск Ресторанов и онлайн
                Бронирование мест в Ресторанах на условиях, предусмотренных настоящим Соглашением.
            </p>
            <p>Использование Системы разрешается только на условиях настоящего Соглашения. Если Пользователь не
                принимает условия Соглашения в полном объёме, Пользователь не имеет права использовать Систему в
                каких-либо целях. Использование Системы с нарушением (невыполнением) какого-либо из условий
                Соглашения запрещено.
            </p>
            <p>Действие настоящего Соглашения распространяется на все последующие обновления/новые версии
                Системы, в том числе Мобильных приложений. Соглашаясь с установкой обновления/новой версии
                Системы, в том числе Мобильных приложений, Пользователь принимает условия настоящего Соглашения
                для соответствующих обновлений/новых версий Системы, в том числе Мобильных приложений, если
                обновление/установка новой версии Системы не сопровождается иным соглашением.
            </p>
            <p><strong>3. УСЛОВИЯ ИСПОЛЬЗОВАНИЯ СИСТЕМЫ Kayvoni</strong></p>
            <p>3.1. Общие условия использования Системы</p>
            <p>3.1.1. Пользователем Системы может быть физическое лицо, достигшее 18 (восемнадцать) лет.</p>
            <p>3.1.2. Лицо становится Пользователем и идентифицируется в Системе, совершив одно из следующих
                действий:</p>
            <div>
                <p>Регистрация в Системе с использованием Номера телефона и полученного на него от Администрации Кода
                    подтверждения;
                </p>
                <p>Подтверждение ознакомления с &laquo;Условиями использования&raquo;, предоставленными системой при
                    регистрации.
                </p>
            </div>
            <p>3.1.3. При использовании Системы Пользователь обязуется:</p>
            <div>
                <p>Предоставлять актуальные и достоверные сведения при Регистрации, а также при дальнейшем
                    использовании Системы. В случае изменения указанных сведений Пользователь обязан незамедлительно
                    уведомить Администрацию об изменении сведений, в том числе об утрате и прекращении срока действия
                    Номера телефона, привязанного к Системе. Администрация не несет ответственности за неблагоприятные
                    последствия, наступившие для Пользователя в результате не уведомления о соответствующих изменениях;
                </p>
                <p>Соблюдать меры безопасности и не передавать пароль и логин для использования Системы третьим
                    лицам.
                </p>
            </div>
            <p>3.1.4. Администрация вправе ограничить полностью или частично доступ к Системе любому Пользователю без
                уведомления в случаях, если Пользователь:</p>
            <div>
                <p>Не соблюдает условия настоящего Соглашения;</p>
                <p>Злоупотребляет какими-либо правами, предоставляемыми Пользователю;</p>
                <p>предоставляет недостоверную информацию, в том числе информацию, вводящую в заблуждение, или
                    неправильные сведения Администрации и Ресторанам.
                </p>
            </div>
            <div>
                <p><strong>Бронирование мест в Ресторанах</strong></p>
                <div>
                    <p>При использовании Системы Пользователям посредством Мобильного приложения Kayvoni
                        предоставляется возможность осуществлять поиск Ресторанов и онлайн Бронирование мест в
                        Ресторанах, информация о которых содержится в Системе.
                    </p>
                    <p>Предварительное бронирование места в ресторане - услуга платная и сумма оплаты определяется
                        администрацией.
                    </p>
                    <p>Основные требования для бронирования места (столика) заранее в ресторане:</p>
                </div>
            </div>
            <div>
                <p>Указать точное время бронирования (день, месяц, час);</p>
                <p>Точное указания количества посетителей;</p>
                <p>Забронировать место как минимум за час до запланированного времени;</p>
                <p>В случае дополнительных запросов или требований пользователя, это должно быть указано в комментариях
                    при оформлении заказа пользователем.
                </p>
            </div>
            <div>
                <div>
                    <p>Система не несет ответственности за случаи, связанные с предварительным бронированием в
                        ресторанах или кафе:
                    </p>
                </div>
            </div>
            <p>Сумма, уплаченная пользователем за предварительное бронирование места в ресторане или кафе, не
                возвращается;</p>
            <div>
                <p>Количество посетителей, указанное при предварительном бронировании в ресторане или кафе, не может
                    быть изменено после оплаты заказа;
                </p>
                <p>Время (день, час, месяц), указанное заранее при бронировании места (столика) в ресторане или кафе,
                    не может быть изменено после оплаты заказа.
                </p>
            </div>
            <div>
                <div>
                    <p>Дополнительные предупреждения:</p>
                </div>
            </div>
            <div>
                <p>Если пользователь не прибыл в ресторан или кафе вовремя, бронь действует не более 30 (тридцать)
                    минут;
                </p>
                <p>Плата за обслуживание и другие дополнительные расходы в ресторане или кафе не оплачивается через
                    систему, а оплата производится в самом ресторане или кафе.
                </p>
            </div>
            <div>
                <p><strong>Предзаказ</strong></p>
            </div>
            <p>5.1. Основные правила при предварительном заказе еды из ресторана или кафе:</p>
            <div>
                <p>Пользователь имеет право оформить предварительный заказ еды только из одного ресторана или кафе за
                    одну транзакцию через систему;
                </p>
                <p>На усмотрение ресторана или кафе устанавливается сумма депозита, которая является минимальной суммой
                    для получения заказа;
                </p>
                <p>Невозможно изменить тип еды после того, как пользователь оплатил заказ;</p>
                <p>В случае отсутствия одного или несколько из указанных пользователем блюд - заказ будет отменен
                    рестораном или кафе.
                </p>
            </div>
            <div>
                <p><strong>Порядок оплаты</strong></p>
                <div>
                    <p>Плата за пользование любой услугой в системе осуществляется через систему Payme.</p>
                </div>
            </div>
            <div>
                <p><strong>Срок действия договора</strong></p>
            </div>
            <p>7.1. Настоящее Соглашение вступает в силу с момента принятия Пользователем условий настоящего
                Соглашения.</p>
            <p>7.2. Администрация имеет право расторгнуть или изменить договор по своему усмотрению.</p>
            <div>
                <p><strong>Ответственность сторон.</strong></p>
            </div>
            <p>8.1. Сторона, не выполнившая полностью или надлежащим образом обязательства по настоящему Соглашению,
                несет ответственность в соответствии с действующим законодательством Республики Узбекистан.
                Администрация не несет ответственности по обязательствам, не предусмотренным настоящим соглашением.</p>
            <div>
                <p><strong>Порядок разрешения споров.</strong></p>
            </div>
            <p>9.1. Все споры и разногласия, вытекающие из настоящего Соглашения, разрешаются в первую очередь по
                соглашению сторон на основании Соглашения.</p>
            <p>9.2. Споры, которые не могут быть разрешены по соглашению всех сторон, разрешаются судом в порядке,
                установленном действующим законодательством Республики Узбекистан.</p>
            <p id="uz" class="mt-9"><strong>"Kayvoni" tizimidan foydalanish kelishuvi</strong></p>
            <p>Kayvoni mobil ilovasidan foydalanish uchun (keying o`rinlarda - Tizim)
                UNITED RESERVATION SYSTEM MCHJ (keyingi o'rinlarda - "Ma'muriyat") birinchi tarafdan va
                tizimdan ro`yaxatdan o`tish uchun ma`lumot kiritgan shaxs (keyingi o'rinlarda -
                "Foydalanuvchi") boshqa tarafdan, Bigalikda Taraflar, ushbu shartnoma (keyingi o'rinlarda
                - "Shartnoma")ni tuzdilar, ushbu shartnoma taraflar o'rtasidagi munosabatlarni tartibga soladi.</p>
            <p>Ushbu Shartnoma O`zbekiston Respublikasi Fuqarolik Kodeksiga hamda boshqa O`zbekiston
                Respublikasining amaldagi qonunchiligiga muvofiq Ma`muriyat tomonidan Kayvoni
                tizimidan foydalanadigan har qanday ikkinchi tomonga yuborilgan ommaviy oferta taklif hisoblanadi. Ushbu
                ommaviy ofertani aktseplash foydalanuvchi tomonidan tizimdan ro`yxatdan o`tish bilan amalga
                oshirilgan hisoblanadi.</p>
            <p>Iltimos, Kayvonitizimidan foydalanishdan oldin ushbu Shartnoma shartlarini diqqat bilan
                o'qing. Agar siz ushbu Shartnoma shartlariga rozi bo'lmasangiz, siz Tizimdan foydalana olmaysiz.</p>
            <p><strong>1. Terminlar va tushunchalar</strong></p>
            <p>1.1 Kayvoni Tizimi (Tizim) Restoran va kafelarni qidirish va ulardan oldindan joy buyurtma qilish
                uchun mo`ljallangan mobil to'lov tizimi, mobil ilova.</p>
            <p>1.2 Kayvoni mobil dasturi O`zbekiston Respublikasining amaldagi qonunchiligiga muvofiq EHM
                uchun mo`ljallangan mobil ilova, mobil qurilmalarga (mobil telefon, smartfon, planshet va
                boshqalar) o`rnatish uchunhamda "iOS" / "Android" operatsion sistemalarida foydalanish uchun
                mo`ljallangan
                ilova .</p>
            <p>1.3Ma'muriyat "UNITED RESERVATION SYSTEM" MCHJ, INN 307905283, ro'yxatdan o'tgan manzil: KICHIK
                XALQA YO`LI, 89A, 33. Kayvoni mobil ilovasiga to`iq egallik qilish huquqiga ega tashkilot.</p>
            <p>1.4 Foydalanuvchi (Mehmon) quyidagi shartnomaga muvofiq foydalanish uchun tizimda ro'yxatdan
                o'tgan jismoniy shaxs.</p>
            <p>1.5 Restoran, kafe (muassasa) - yuridik yoki yakka tartibdagi tadbirkor, restoran, kafe yoki boshqa
                turdagi muassasa, o`z ma`lumotlarini Tovar yoki xizmatlarini tizimda joylashtirish huquqiga
                ega tashkilot.</p>
            <p>1.6 Tizimda ro'yxatdan o'tish foydalanuvchi tomondan birinchi kirishda qo'llaniladigan
                protsedura, shu orqali foydalanuvchi ilova ushbu shartlarni to'liq va so'zsiz qabul qilganligini
                bildiradi. Tizimga telefon raqamga kelgan tasdiqlash kodini kiritish orqali amalga oshiriladi.</p>
            <p>1.7 Telefon raqami - Uyali aloqa operatori tomonidan Foydalanuvchiga tayinlangan telefon raqami</p>
            <p>1.8 Tasdiqlash kodi raqamlar ketma-ketligi (parol) ma'muriyat tomonidan foydalanuvchi telefon
                raqamiga SMS-xabarnoma xizmati orqali, ro`yhatdan o`tish vaqtida bir daqiqa ichida
                yuboriladigan SMS-xabar.</p>
            <p>1.9 Band qilish Kayvoni tizimi funksiyasi, restoranlarda oldindan joy (stol) buyurtma qilish.</p>
            <p>1.10. Oferta Bir yoki bir necha muayan shaxsga yuborilgan, yetarli darajada aniq bo`lgan va
                taklifni kiritgan shaxsning o`zini taklif yo`llangan va uni qabul qiladigan shaxs bilan
                shartnoma tuzgan deb hisoblash niyatini ifoda etadigan taklif.</p>
            <p>1.11. Aksept Oferta yuborilgan shaxsning uni qabul qilganligi haqidagi javobi.</p>
            <p><strong>2. Shartnomaning predmeti</strong></p>
            <p>2.1. Ushbu Shartnomaga binoan Ma'muriyat Foydalanuvchiga tizimdan doydalanish huquqini taqdim etadi.
                Tizim orqali restoran va kafelarni qidirish, tanlash, oldindan joy buyurtma berish, shuningdek tizim
                orqali restoran va kafe menyularida ko`rsatilgan taomlarga oldindan buyurtma berish mumkin
                (quyidagi shatnoma shartlari asosida)</p>
            <p>2.2. Tizimdan foydalanishga faqat ushbu Shartnoma shartlariga binoan ruxsat beriladi. Agar Foydalanuvchi
                Shartnoma shartlarini to'liq qabul qilmaydigan holatda, Foydalanuvchining tizimdan foydalanish huquqi
                yo`q
                deb qaraladi.</p>
            <p>2.3. Ushbu Shartnoma keyingi tizimdagi barcha yangilanishlar / yangi versiyalar uchun amal qiladi.</p>
            <p><strong>3. Kayvoni tizimidan foydalanish shartlari</strong></p>
            <p>3.1.1 Tizimdan foydalanishning umumiy shartlari</p>
            <p>3.1.2 Tizim foydalanuvchisi 18 (o'n sakkiz) yoshga to'lgan shaxs bo'lishi mumkin.</p>
            <p>3.1.3 Shaxslar tizimdan quyidagi holatda ro`yhatdan o`tish orqali tizim foydalanuvchilari
                hisoblanishadi:</p>
            <p>Telefon raqami va tizim mamuriyati tomonidan telefon raqamiga kelgan kodni kiritish orqalitizimda
                ro'yxatdan o'tish;</p>
            <p>Registratsiya vaqtida tizim tomonidan taqdim etilgan Foydalanish shartlari bilan
                tanishib chiqqanligini tasdiqlash.</p>
            <p>3.2.1 Tizimdan foydalanishda foydalanuvchi quyidagilarni o'z zimmasiga oladi:</p>
            <p>Ro`yhatdan o`tish vaqtida tizimga to`g`ri ma`lumotlarni kiritish, o`ziga
                tegishli bo`lgan telefon raqamni kiritish shuningdek ro`yhatdan o`tish yakunlangandan
                so`ng foydalanuvchi ma`lumotlari o`zgargan taqdirda bu haqida tizim ma`muriyatiga
                habar berish;</p>
            <p>Xavfsizlik choralariga rioya qilish va tizimdan foydalanish uchun taqdim etilgan parol va loginni
                uchinchi shaxslarga ko`rsatmaslik, bermaslik.</p>
            <p>3.2.2 Ma'muriyat quyidagi holatlarni keltirib chiqargan istalgan foydalanuvchiga tizimga kirishni to'liq
                yoki qisman cheklash huquqiga ega:</p>
            <p>ushbu Shartnoma shartlariga rioya qilmasa;</p>
            <p>foydalanuvchiga berilgan har qanday huquqlarni suiste'mol qilsa;</p>
            <p>ma'muriyat va restoranlarga noto'g'ri ma'lumotlar, shu jumladan yolg`on ma`lumotlar
                taqdim etsa.</p>
            <p><strong>4. Restoran yoki kafelarda oldindan joy (Stol) band qilish shartlari</strong></p>
            <p>4.1. Restoran yoki kafelarda oldindan joy (stol) band qilish pullik xizmat hisoblanib uni to`lash
                majburiy hisoblanadi va to`lov summasi ma`muriyat tominidan belgilanadi.</p>
            <p>4.2. Restoran yoki kafelarda oldindan joy (stol) band qilishdagi asosiy talablar:</p>
            <p>Buyurtma vaqtini (kun, oy, soat ) aniq ko`rsatish;</p>
            <p>Boruvchilar sonini aniq belgilash;</p>
            <p>Restoran yoki kafega borish uchun reja qilinayotgan vaqtdan eng kamida bir soat oldin tizim orqali
                joy band qilish;</p>
            <p>Foydalanuvchining qo`shimcha iltimosi yoki talablari bo`lgan taqdirda foydalanuvchi
                tomonidan buyurtma qilish vaqtida izohda ko`rsatilishi kerak.</p>
            <p>4.3. Tizim restoran yoki kafelarda oldindan joy (Stol) band qilish bilan bog`liq bo`lgan
                quyidagi holatlarni o`z bo`yniga olmaydi:</p>
            <p>Restoran yoki kafelarda oldindan joy (Stol) band qilish uchun foydalanuvchi tomonidan to`langan
                summa qaytarilmaydi;</p>
            <p>Restoran yoki kafelarda oldindan joy (Stol) band qilish vaqtida ko`rsatilgan boruvchilar
                sonini buyurtma uchun to`lov amalga oshirilgandan so`ng o`zgartirib
                bo`lmaydi;</p>
            <p>Restoran yoki kafelarda oldindan joy (Stol) band qilish vaqtida ko`rsatilgan vaqtni (kun,
                soat,oy)buyurtma uchun to`lov amalga oshgandan so`ng ozgartirib bo`lmaydi.</p>
            <p>4.4. Qo`shimcha ogohlantirishlar:</p>
            <p>Foydalanuvchi restoran yoki kafega vaqtida bormagan taqdirda buyurtma 30 (o`ttiz) daqiqa
                davomida amal qiladi;</p>
            <p>Restoran yoki kafe xizmat ko`rsatish haqqi tizim orqali amalga oshirilmaydi va uning uchun
                to`lovni restoran yoki kafening o`zida amalga oshiriladi.</p>
            <p><strong>5. Restoran yoki kafelardan oldindan taom buyurtma qilish bo`yicha shartar</strong></p>
            <p>5.1. Restoran yoki kafelardan oldindan taom buyurtma qilish vaqtida asosiy qoidalar:</p>
            <p>Foydalanuvchi tizim orqali bir tranzaksiya uchun faqat bir restoran yoki kafedan taom uchun
                oldindan buyurtma qoldirish huquqiga ega;</p>
            <p>Har bir restoran yoki kafe talabidan kelib chiqib depozit summa belgilangan bo`lib, bu summa
                buyurtma qabul qilish uchun eng kam belgilangan summa hisoblanadi;</p>
            <p>Foydalanuvchi tomonidan buyurtma uchun to`lov amalga oshganidan so`ng taom turini o`zgartirib
                bo`lmaydi;</p>
            <p>Foydalanuvchi tomonidan belgilangan taomlardan biri mavjud bo`lmagan taqdirda restoran yoki
                kafe tomonidan buyurtma bekor qilinadi (to`lov amalga oshmay turib).</p>
            <p><strong>6. To`lovlarni amalga oshirish tartibi</strong></p>
            <p>6.1 Tizimda har qanday xizmat uchun foydalanuvchidan undiriladigan to`lovlar Payme
                tizimi orqali amalga oshiriladi.</p>
            <p><strong>7. Shartnoma amal qilishi</strong></p>
            <p>7.1. Ushbu Shartnoma Foydalanuvchi Shartnoma shartlarini qabul qilgan paytdan boshlab kuchga kiradi.</p>
            <p>7.2. Ma`muriyat bir taraflama shartnomani bekor qilish yoki o`zgartirish huquqiga ega.</p>
            <p><strong>8. Tomonlar javobgarligi.</strong></p>
            <p>8.1. Ushbu shartnomada nazarda tutilgan majburiyatlarni to`liq yoki yetarli darajada bajarmagan
                tarafga O`zbekiston Respublikasi amaldagi qonunchiligiga ko`ra javobgarlik qo`llanadi.
                Ushbu shartnoma bo`cha ma`muriyat bo`yniga olmagan majburiyatlar, holatlar boyicha ma`muriyat
                javobgarikga tortilmaydi.</p>
            <p><strong>9. Nizoni hal qilish tartibi.</strong></p>
            <p>9.1. Ushbu shartnomadan kelib chiqgan barcha nizo va kelishmovchiliklar eng avvalo taraflarning kelishuvi
                bilan hal qilinadi.</p>
            <p>9.2. Taraflarni kelishuvi bilan hal qilib bo`lmaydigan nizolar O`zbekiston Respublikasining
                amaldagi qonunchiligida belgilangan tartibda sudi orqali hal qilinadi.</p>
            <p id="en" class="mt-9"><strong>Agreement on the use of the "Kayvoni" mobile app system</strong></p>
            <p>To use the mobile application "Kayvoni" (hereinafter referred to as the "System") LLC "UNITED RESERVATION
                SYSTEM (hereinafter referred to as the "System Administration"), as the First Party, and the
                person who enters the information for registration in the System (hereinafter referred to as the
                "User"), as the Second Party, which is hereinafter collectively referred to as "Parties", have concluded
                this Agreement (hereinafter referred to as the "Agreement"), which regulates the relationship between
                the Parties.</p>
            <p>This Agreement is a public offer proposed by the System Administration to any other Party that uses the
                Kayvoni system in accordance with the Civil Code of the Republic of Uzbekistan and other current
                legislation of the Republic of Uzbekistan. This public offer is considered to have been accepted by the
                User upon registration in the System.</p>
            <p>Please read the terms of this Agreement carefully before using the Kayvoni Mobile App System. If you do
                not agree to the terms of this Agreement, you will not be able to use the System.</p>
            <p><strong>1. Terms and definitions</strong></p>
            <p>1.1 Kayvoni System (System) - a mobile payment system for searching restaurants and cafes and reserving
                tables in them, a mobile application.</p>
            <p>1.2 The Kayvoni mobile application -a mobile app developed for ECM in accordance with the current
                legislation of the Republic of Uzbekistan, for installation on mobile devices (mobile phones,
                smartphones, tablets, etc.) and for use in iOS / Android operating systems.</p>
            <p>1.3 Administration - UNITED RESERVATION SYSTEM LLC (TIN - 307905283, legal address: KKICHIK XALQA YO`LI,
                89A, 33.) is an entity that has full ownership of the Kayvani mobile app.</p>
            <p>1.4 User (Guest) - an individual registered in the System for use in accordance with the this
                Agreement.</p>
            <p>1.5 Restaurant, cafe (facility) - a legal or individual entrepreneur, restaurant, cafe or other facility,
                an entity that has the right to post information about its goods or services in the System.</p>
            <p>1.6 Registration in the System - the procedure followed by the User at the first registering in the
                System, by which the User confirms that the application has fully and unconditionally accepted these
                terms. This is done by entering a verification code sent to the User`s phone number in the System.</p>
            <p>1.7 Phone number - the phone number assigned to the User by the mobile operator.</p>
            <p>1.8 Confirmation code - a sequence of numbers (password) sent by the Administration to the User's phone
                number through the SMS notification service within one minute after the start of the registration
                process.</p>
            <p>1.9 Reservation - a feature of the Kayvoni System, preliminary reservation of a place (table) in
                restaurants.</p>
            <p>1.10. Offer - an offer addressed to one or several specific persons to conclude an Agreement, which quite
                definitely expresses the intention of the person who made the offer, to consider himself to have entered
                into an Agreement with the Addressee who will accept the offer.</p>
            <p>1.11. Acceptance - the response of the person to whom the offer is addressed about its acceptance.</p>
            <p><strong>2. Subject of the Agreement.</strong></p>
            <p>2.1. Under this Agreement, the Administration grants the User the right to use the System. The System
                allows the User to search, select restaurants and cafes, reserve a table in them, as well as pre-order
                foods in the menu of restaurants and cafes (under the terms of the this Agreement)</p>
            <p>2.2. The use of the System is permitted only in accordance with the terms of this Agreement. If the User
                does not fully accept the terms of the Agreement, the User will not be able to use the system.</p>
            <p>2.3. This Agreement applies to all subsequent updates to the System / new versions of this System.</p>
            <p><strong>3. Terms of use of the Kayvoni mobile app system</strong></p>
            <p>3.1.1 General Terms of the Use of the System</p>
            <p>3.1.2 The User of the System shall be a person over 18 (eighteen) years old.</p>
            <p>3.1.3 Individuals are considered the Users of the System by registering in the System in the following
                cases:</p>
            <p>to register in the System by entering the phone number and the code sent by the System
                Administration to the phone number;</p>
            <p>to confirm that you have read the "Terms of Use" provided by the System at the time of
                registration.</p>
            <p>3.2.1 When using the System, the User undertakes to:</p>
            <p>Enter the correct information and personal phone number in the System at the time of registration,
                as well as notify the System Administration in case of change of User`s data after the registration;</p>
            <p>To comply with security measures and not to show or give the provided password and login to third
                Parties to use the System.</p>
            <p>3.2.2 The Administration reserves the right to completely or partially restrict access to any User who
                causes the following cases. If the User:</p>
            <p>fails to comply with the terms of this Agreement;</p>
            <p>abuses any rights granted to the User;</p>
            <p>provides incorrect information, including false information, to the Administration and
                restaurants.</p>
            <p><strong>4. Terms of reserving places (tables) in restaurants or cafes</strong></p>
            <p>4.1. Reserving a table in a restaurant or cafe is a paid service and is mandatory.</p>
            <p>4.2. The main terms for reserving table in advance in a restaurant or cafe:</p>
            <p>Clear indication of order time (day, month, hour);</p>
            <p>Precise numbering of visitors;</p>
            <p>To reserve a table through the System at least one hour before the scheduled time to go to a
                restaurant or cafe;</p>
            <p>In case of additional requests or claims of the User, it should be indicated in the comments at
                the time of ordering by the user.</p>
            <p>4.3. The System does not accept any responsibility for the following cases related to reserving a place
                (table) in advance in a restaurant or cafe:</p>
            <p>The amount paid by the User for reserving a table in a restaurant or cafe is not refunded;</p>
            <p>The number of visitors indicated at the time of reservation in advance in a restaurant or cafe
                (Table) cannot be changed after payment for the placed order;</p>
            <p>In restaurants or cafes, the time (day, hour, month) specified during the reservation of the place
                (table) can not be changed after payment for the placed order.</p>
            <p>4.4. Additional warnings:</p>
            <p>The order is valid for 30 (thirty) minutes if the User does not visit the restaurant or cafe on
                time;</p>
            <p>Restaurant or cafe service fees are not paid through the System and payment is made at the
                restaurant or cafe itself.</p>
            <p><strong>5. Terms for ordering food from a restaurant or cafe</strong></p>
            <p>5.1. Basic rules when ordering food in advance from a restaurant or cafe;</p>
            <p>The User has the right to place a pre-order for a meal from only one restaurant or cafe for per
                transaction through the System;</p>
            <p>Depending on the demand of each restaurant or cafe, a deposit amount is set, which is the minimum
                amount to receive an order;</p>
            <p>The type of food cannot be changed after the User has paid for the order;</p>
            <p>In the absence of one of the foods specified by the user, the order will be canceled by the
                restaurant or cafe (before receiving the payment from the System User).</p>
            <p><strong>6. Payment procedure</strong></p>
            <p>6.1 User`s charges for any service in the System are made through the "Payme" app.</p>
            <p><strong>7. Enforcement of the Agreement</strong></p>
            <p>7.1. This Agreement shall enter into force upon the User's acceptance of the terms of this Agreement.</p>
            <p>7.2. The System Administration has the right to unilaterally terminate or amend the Agreement.</p>
            <p><strong>8. Liability of the Parties.</strong></p>
            <p>8.1. A party who has not fully or insufficiently fulfilled the obligations provided in this Agreement
                shall be liable in accordance with the current legislation of the Republic of Uzbekistan. The System
                Administration is not liable for any obligations not covered by this Agreement.</p>
            <p><strong>9. Dispute Resolution Procedure.</strong></p>
            <p>9.1. All disputes and disagreements arising from this Agreement shall be settled primarily by accord of
                the Parties.</p>
            <p>9.2. Disputes that cannot be resolved by accord of the Parties shall be resolved by a court in accordance
                with the procedure established by the current legislation of the Republic of Uzbekistan.</p>
        </div>
    </section>
@endsection
