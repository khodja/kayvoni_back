@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <form class="form" action="{{ route('slider.update',$data->id) }}" method="POST"
                      enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="white-block mb-30">
                        <div class="head">
                            <h3>Изменить</h3>
                        </div>
                        <div class="content">
                            <div class="input-block">
                                <div class="input">
                                    <label>Загрузите для обновления фотографии 640x360:</label>
                                    <input type="file" name="image[]" class="image">
                                    <div class="d-flex justify-content-between mt-5">
                                        @foreach($images as $image)
                                            <img src="{{asset($image)}}?time={{microtime(true)}}" alt="image"
                                                 width="150"/>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="button-block">
                        <button type="submit" class="continue-btn">Добавить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
