@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <form class="form" action="{{ route('slider.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="white-block mb-30">
                        <div class="head">
                            <h3>Добавить Баннер</h3>
                        </div>
                        <div class="content">
                            <div class="input-block">
                                <div class="input">
                                    <label>Фото 640x360:</label>
                                    <input type="file" required="required" name="image[]">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="button-block">
                        <button type="submit" class="continue-btn">Добавить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
