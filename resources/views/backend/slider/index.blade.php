@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <h2 class="blue-title">Баннеры</h2>
            <div class="row">
                <div class="form">
                    <div class="white-block hotels">
                        <ul class="list-block">
                            @foreach($data as $key => $datas)
                                <li class="item">
                                    <a class="left-block" href="{{ route('slider.edit' , $datas->id) }}">
                                        <p class="pl-2">№ {{ $datas->id  }}</p>
                                    </a>
                                    <div class="right-block">
                                        <a href="{{ route('slider.edit' , $datas->id) }}" class="btn btn-primary">
                                            <i class="fe fe-edit"></i>
                                        </a>
                                        @if($key != 0)
                                            <form action="{{route('slider.delete',$datas->id)}}"
                                                  class="d-inline-flex ml-1" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button class="btn btn-danger">
                                                    <i class="fe fe-trash"></i>
                                                </button>
                                            </form>
                                        @endif
                                    </div>
                                </li>
                            @endforeach
                            <a class="add-list-btn" href="{{ route('slider.create') }}">
                                <i class="fe fe-plus-circle"></i>
                                Добавить
                            </a>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
