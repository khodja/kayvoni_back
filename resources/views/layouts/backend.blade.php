<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="ltr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Language" content="en"/>
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#4188c9">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="msapplication-TileImage" content="/apple-touch-icon-180x180.png">
    <meta name="application-name" content="Fulibu">
    <title>Kayvoni System</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext">
    @yield('style')

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
    <link rel="apple-touch-icon" href="/apple-touch-icon.png"/>
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png"/>
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png"/>
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png"/>
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png"/>
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png"/>
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png"/>
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png"/>
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png"/>

    <script src="{{ asset('backend/js/require.min.js')}}"></script>
    <script src="{{ asset('backend/js/vendors/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('backend/plugins/filer/jquery.filer.js') }}"></script>
    <link rel="stylesheet" href="{{asset('backend/css/jquery.filer.css')}}">
    <link rel="stylesheet" href="{{asset('backend/css/jquery.filer-dragdropbox-theme.css')}}">
    <script>
        requirejs.config({
            baseUrl: '/'
        });
    </script>
    <link href="{{ asset('backend/css/dashboard.css')}}" rel="stylesheet"/>
    <link href="{{ asset('backend/css/bootstrap-timepicker.min.css')}}" rel="stylesheet"/>
    <script src="{{ asset('backend/js/dashboard.js')}}"></script>
    <link href="{{ asset('backend/plugins/charts-c3/plugin.css')}}" rel="stylesheet"/>
    <script src="{{ asset('backend/plugins/charts-c3/plugin.js')}}"></script>
    <link href="{{ asset('backend/plugins/maps-google/plugin.css')}}" rel="stylesheet"/>
    <script src="{{ asset('backend/plugins/maps-google/plugin.js')}}"></script>
    <script src="{{ asset('backend/plugins/input-mask/plugin.js')}}"></script>
    <script src="{{ asset('backend/plugins/datatables/plugin.js') }}"></script>
    <script src="{{ asset('backend/js/custom.js')}}"></script>
    <script src="{{ asset('backend/js/main.js')}}"></script>
    <link href="{{ asset('backend/css/custom.css')}}" rel="stylesheet"/>
    <link href="{{ asset('backend/css/style.css')}}" rel="stylesheet"/>
</head>
<body>
<div class="page">
    <div class="flex-fill">
    @auth
        <header class="second">
            <div class="top-block">
                <div class="container">
                    <div class="row">
                        <div class="left-block">
                            <a href="{{ route('home.index') }}">
                                <img src="{{ asset('backend/images/logo.svg') }}" alt="logo" height="50"/>
                            </a>
                            <span class="description">Kayvoni System</span>
                        </div>
                        <div class="right-block">
                            <div class="dropdown">
                                <a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown">
                                <span class="avatar">
                                    <svg width="32" height="32"
                                         viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <rect width="32" height="32" rx="16" fill="#43F4EE"/>
                                    <path
                                        d="M22.6668 23.5V21.8333C22.6668 20.9493 22.3156 20.1014 21.6905 19.4763C21.0654 18.8512 20.2176 18.5 19.3335 18.5H12.6668C11.7828 18.5 10.9349 18.8512 10.3098 19.4763C9.68469 20.1014 9.3335 20.9493 9.3335 21.8333V23.5"
                                        stroke="#00514E" stroke-width="1.5" stroke-linecap="round"
                                        stroke-linejoin="round"/>
                                    <path
                                        d="M15.9998 15.1667C17.8408 15.1667 19.3332 13.6743 19.3332 11.8333C19.3332 9.99238 17.8408 8.5 15.9998 8.5C14.1589 8.5 12.6665 9.99238 12.6665 11.8333C12.6665 13.6743 14.1589 15.1667 15.9998 15.1667Z"
                                        stroke="#00514E" stroke-width="1.5" stroke-linecap="round"
                                        stroke-linejoin="round"/>
                                    </svg>
                                </span>
                                    <span class="ml-2 d-none d-lg-block">
                                    <span class="text-default">{{ Auth::user()->name }}</span>
                                </span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                    <form method="POST" action="{{ route('user.logout') }}">
                                        @csrf
                                        <button class="dropdown-item" type="submit"><i
                                                class="dropdown-icon fe fe-log-out"></i> Выйти
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom-block">
                <div class="container">
                    <ul class="row justify-content-start">
                        <li class="nav-item">
                            <a href="{{ route('home.index') }}"
                               class="nav-link {{ request()->is('dashboard') ? 'active' : '' }}">
                                <i class="fe fe-activity mr-2"></i>
                                Статистика</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('category.index') }}"
                               class="nav-link {{ request()->is('*category*') ? 'active' : '' }}">
                                <i class="fe fe-layers mr-2"></i>
                                Категории</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('slider.index') }}"
                               class="nav-link {{ request()->is('*slider*') ? 'active' : '' }}">
                                <i class="fe fe-airplay mr-2"></i>
                                Слайдер</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('restaurant.index') }}"
                               class="nav-link {{ request()->is('*restaurant*') ? 'active' : '' }}">
                                <i class="fe fe-home mr-2"></i>
                                Рестораны</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('sort.index') }}"
                               class="nav-link {{ request()->is('*sorts*') ? 'active' : '' }}">
                                <i class="fe fe-info mr-2"></i>
                                Тип ресторанов </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('type.index') }}"
                               class="nav-link {{ request()->is('*type*') ? 'active' : '' }}">
                                <i class="fe fe-package mr-2"></i>
                                Информация Ресторанов</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('informations.index') }}"
                               class="nav-link {{ request()->is('*informations*') ? 'active' : '' }}">
                                <i class="fe fe-info mr-2"></i>
                                Причины отказа ресторанов </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('city.index') }}"
                               class="nav-link {{ request()->is('*region*') ? 'active' : '' }}">
                                <i class="fe fe-map mr-2"></i>
                                Регионы</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('order.index') }}"
                               class="nav-link {{ request()->is('*orders*') ? 'active' : '' }}">
                                <i class="fe fe-info mr-2"></i>
                                Заказы </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('manager.index') }}"
                               class="nav-link {{ request()->is('*manager*') ? 'active' : '' }}">
                                <i class="fe fe-user mr-2"></i>
                                Менеджеры </a>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
        @endauth
        @include('partials.message')
        @yield('content')
    </div>
    <footer class="footer">
        <div class="container">
            <div class="row align-items-center flex-row-reverse">
                <div class="col-12 mt-3 mt-lg-0 text-center">
                    Copyright © {{ \Carbon\Carbon::now()->year }}. Все права защищены.
                </div>
            </div>
        </div>
    </footer>
</div>
@yield('script')
</body>
</html>
