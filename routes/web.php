<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\SliderController;
use App\Http\Controllers\RestaurantController;
use App\Http\Controllers\CityController;
use App\Http\Controllers\TypeController;
use App\Http\Controllers\InformationController;
use App\Http\Controllers\FoodController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\FoodParamsController;
use App\Http\Controllers\PromoController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\SortController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/policy', [SliderController::class, 'policy']);
Route::group(['middleware' => ['role:admin']], function () {
    Route::get('dashboard', [HomeController::class, 'index'])->name('home.index');
    Route::post('user/logout', [HomeController::class, 'logout'])->name('user.logout');

    Route::get('dashboard/refresh', [HomeController::class, 'refresh'])->name('home.refresh');
    Route::get('dashboard/clearCache', [HomeController::class, 'refresh'])->name('home.clearCache');

    Route::get('dashboard/clearRoute', [HomeController::class, 'clearRoute'])->name('home.clearRoute');
    Route::get('dashboard/clearView', [HomeController::class, 'clearView'])->name('home.clearView');
    Route::get('dashboard/clearConfig', [HomeController::class, 'clearConfig'])->name('home.clearConfig');
    Route::get('dashboard/clearOptimize', [HomeController::class, 'clearOptimize'])->name('home.clearOptimize');

    Route::get('dashboard/category', [CategoryController::class, 'index'])->name('category.index');
    Route::get('dashboard/category/show/{id}', [CategoryController::class, 'show'])->name('category.show');
    Route::get('dashboard/category/create', [CategoryController::class, 'create'])->name('category.create');
    Route::get('dashboard/category/edit/{id}', [CategoryController::class, 'edit'])->name('category.edit');
    Route::post('dashboard/category/store', [CategoryController::class, 'store'])->name('category.store');
    Route::put('dashboard/category/update/{id}', [CategoryController::class, 'update'])->name('category.update');
    Route::delete('dashboard/category/delete/{id}', [CategoryController::class, 'delete'])->name('category.delete');
//
    Route::get('dashboard/type', [TypeController::class, 'index'])->name('type.index');
    Route::get('dashboard/type/create', [TypeController::class, 'create'])->name('type.create');
    Route::get('dashboard/type/edit/{id}', [TypeController::class, 'edit'])->name('type.edit');
    Route::post('dashboard/type/store', [TypeController::class, 'store'])->name('type.store');
    Route::put('dashboard/type/update/{id}', [TypeController::class, 'update'])->name('type.update');
    Route::delete('dashboard/type/delete/{id}', [TypeController::class, 'delete'])->name('type.delete');
//
    Route::get('dashboard/informations', [InformationController::class, 'index'])->name('informations.index');
    Route::get('dashboard/informations/create', [InformationController::class, 'create'])->name('informations.create');
    Route::get('dashboard/informations/edit/{id}', [InformationController::class, 'edit'])->name('informations.edit');
    Route::post('dashboard/informations/store', [InformationController::class, 'store'])->name('informations.store');
    Route::put('dashboard/informations/update/{id}', [InformationController::class, 'update'])->name('informations.update');
    Route::delete('dashboard/informations/delete/{id}', [InformationController::class, 'delete'])->name('informations.delete');
//
    Route::get('dashboard/sorts', [SortController::class, 'index'])->name('sort.index');
    Route::get('dashboard/sorts/create', [SortController::class, 'create'])->name('sort.create');
    Route::get('dashboard/sorts/edit/{id}', [SortController::class, 'edit'])->name('sort.edit');
    Route::post('dashboard/sorts/store', [SortController::class, 'store'])->name('sort.store');
    Route::put('dashboard/sorts/update/{id}', [SortController::class, 'update'])->name('sort.update');
    Route::delete('dashboard/sorts/delete/{id}', [SortController::class, 'delete'])->name('sort.delete');

//
    Route::get('dashboard/slider', [SliderController::class, 'index'])->name('slider.index');
    Route::get('dashboard/slider/create', [SliderController::class, 'create'])->name('slider.create');
    Route::get('dashboard/slider/edit/{id}', [SliderController::class, 'edit'])->name('slider.edit');
    Route::post('dashboard/slider/store', [SliderController::class, 'store'])->name('slider.store');
    Route::put('dashboard/slider/update/{id}', [SliderController::class, 'update'])->name('slider.update');
    Route::delete('dashboard/slider/delete/{id}', [SliderController::class, 'delete'])->name('slider.delete');
//
    Route::get('dashboard/restaurant', [RestaurantController::class, 'index'])->name('restaurant.index');
    Route::get('dashboard/restaurant/create', [RestaurantController::class, 'create'])->name('restaurant.create');
    Route::get('dashboard/restaurant/edit/{id}', [RestaurantController::class, 'edit'])->name('restaurant.edit');
    Route::post('dashboard/restaurant/store', [RestaurantController::class, 'store'])->name('restaurant.store');
    Route::put('dashboard/restaurant/update/{id}', [RestaurantController::class, 'update'])->name('restaurant.update');
    Route::get('dashboard/restaurant/switch/{id}', [RestaurantController::class, 'switch'])->name('restaurant.switch');
    Route::delete('dashboard/restaurant/delete/{id}', [RestaurantController::class, 'delete'])->name('restaurant.delete');
    Route::get('dashboard/restaurant/remove/image/{id}', [RestaurantController::class, 'removeImage'])->name('restaurant.remove');
//
    Route::get('dashboard/food/restaurant/{id}', [FoodController::class, 'index'])->name('food.index');
    Route::get('dashboard/food/restaurant/{id}/create', [FoodController::class, 'create'])->name('food.create');
    Route::get('dashboard/food/edit/{id}', [FoodController::class, 'edit'])->name('food.edit');
    Route::post('dashboard/food/restaurant/{id}/store', [FoodController::class, 'store'])->name('food.store');
    Route::put('dashboard/food/restaurant/{id}/update', [FoodController::class, 'update'])->name('food.update');
    Route::delete('dashboard/food/delete/{id}', [FoodController::class, 'delete'])->name('food.delete');
    Route::get('dashboard/food/switch/{id}', [FoodController::class, 'switch'])->name('food.switch');
//
    Route::get('dashboard/food/param/{id}', [FoodParamsController::class, 'index'])->name('params.index');
    Route::get('dashboard/food/param/{id}/create', [FoodParamsController::class, 'create'])->name('params.create');
    Route::get('dashboard/param/edit/{id}', [FoodParamsController::class, 'edit'])->name('params.edit');
    Route::post('dashboard/food/param/{id}/store', [FoodParamsController::class, 'store'])->name('params.store');
    Route::put('dashboard/food/param/{id}/update', [FoodParamsController::class, 'update'])->name('params.update');
    Route::delete('dashboard/param/delete/{id}', [FoodParamsController::class, 'delete'])->name('params.delete');
    Route::get('dashboard/param/switch/{id}', [FoodParamsController::class, 'switch'])->name('params.switch');
//
    Route::get('dashboard/promo/restaurant/{id}', [PromoController::class, 'index'])->name('promo.index');
    Route::get('dashboard/promo/restaurant/{id}/create', [PromoController::class, 'create'])->name('promo.create');
    Route::get('dashboard/promo/edit/{id}', [PromoController::class, 'edit'])->name('promo.edit');
    Route::post('dashboard/promo/{id}/store', [PromoController::class, 'store'])->name('promo.store');
    Route::put('dashboard/promo/{id}/update', [PromoController::class, 'update'])->name('promo.update');
    Route::delete('dashboard/promo/delete/{id}', [PromoController::class, 'delete'])->name('promo.delete');
    Route::get('dashboard/promo/switch/{id}', [PromoController::class, 'switch'])->name('promo.switch');
//
    Route::get('dashboard/region', [CityController::class, 'index'])->name('city.index');
    Route::get('dashboard/region/show/{id}', [CityController::class, 'show'])->name('city.show');
    Route::get('dashboard/region/create', [CityController::class, 'create'])->name('city.create');
    Route::get('dashboard/region/edit/{id}', [CityController::class, 'edit'])->name('city.edit');
    Route::post('dashboard/region/store', [CityController::class, 'store'])->name('city.store');
    Route::get('dashboard/region/toggle/{id}', [CityController::class, 'toggleMain'])->name('city.toggleMain');
    Route::put('dashboard/region/update/{id}', [CityController::class, 'update'])->name('city.update');

    Route::get('manager/{id}/rest', [UserController::class, 'restaurant'])->name('manager.restaurant');
    Route::get('manager', [UserController::class, 'index'])->name('manager.index');
    Route::get('manager/create', [UserController::class, 'create'])->name('manager.create');
    Route::get('manager/edit/{id}', [UserController::class, 'edit'])->name('manager.edit');
    Route::post('manager/store', [UserController::class, 'store'])->name('manager.store');
    Route::put('manager/update/{id}', [UserController::class, 'update'])->name('manager.update');

    Route::get('dashboard/orders', [OrderController::class, 'index'])->name('order.index');
    Route::get('dashboard/orders/{id}', [OrderController::class, 'showOrder'])->name('order.show');
});
Auth::routes();
