<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SliderController;
use App\Http\Controllers\RestaurantController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CityController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\FoodController;
use App\Http\Controllers\RatingController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\PromoController;
use App\Http\Controllers\TypeController;
use App\Http\Controllers\InformationController;
use App\Http\Controllers\SortController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/// Users
Route::get('/slider', [SliderController::class, 'get']);
Route::get('/main', [RestaurantController::class, 'get']);
Route::get('/search?', [SearchController::class, 'get']);
Route::get('/category', [CategoryController::class, 'get']);
Route::get('/types', [TypeController::class, 'get']);
Route::get('/sorts', [SortController::class, 'get']);
Route::get('/category/{id}/restaurants', [RestaurantController::class, 'category']);
Route::get('/region', [CityController::class, 'get']);
Route::get('/show/restaurant/{id}', [RestaurantController::class, 'show']);
Route::get('/show/promo/restaurant/{id}', [PromoController::class, 'restaurant']);
Route::get('/show/food/{id}', [FoodController::class, 'show']);
Route::get('/multiple/food', [FoodController::class, 'getFoods']);
Route::get('/show/promo/{id}', [PromoController::class, 'show']);
Route::post('/verification', [AuthController::class, 'verification']);
Route::post('/auth', [AuthController::class, 'auth']);



// Bearer token needed routes
Route::group(['middleware' => ['auth:api']], function () {
    Route::get('/cart/restaurant/{id}', [RestaurantController::class, 'showCart']);
    Route::get('/user/orders', [OrderController::class, 'get']);
    Route::get('/user', [AuthController::class, 'getUser']);
    Route::get('/user/order/{id}', [OrderController::class, 'show']);
    Route::post('/user/checkout', [OrderController::class, 'checkout']);
    Route::put('/set/token', [AuthController::class, 'setToken']);
    Route::post('/order/{orderId}/rating', [RatingController::class, 'store']);
    Route::put('/user/update', [AuthController::class, 'register']);
    Route::get('/logout', [AuthController::class, 'logout']);
});

// Restaurant api

Route::post('/restaurant/auth', [AuthController::class, 'restLogin']);
Route::group(['middleware' => ['role:restaurant', 'auth:api']], function () {
    Route::get('/restaurant/user', [AuthController::class, 'getRestaurantUser']);
    Route::put('/restaurant/token', [AuthController::class, 'setToken']);
    Route::get('/restaurant/reason', [InformationController::class, 'get']);
    Route::get('/restaurant/statistics', [RestaurantController::class, 'statistics']);
    Route::get('/restaurant/food', [FoodController::class, 'get']);
    Route::put('/restaurant/food/{foodId}/switch', [FoodController::class, 'switchApi']);
    Route::get('/restaurant/orders', [OrderController::class, 'restGet']);
    Route::get('/restaurant/show/order/{orderID}', [OrderController::class, 'restShow']);
    Route::put('/restaurant/order/{orderId}/accept', [OrderController::class, 'acceptOrder']);
    Route::put('/restaurant/order/{orderId}/deny', [OrderController::class, 'denyOrder']);
});

Route::post('/payment/checkout', [TransactionController::class, 'run']);
